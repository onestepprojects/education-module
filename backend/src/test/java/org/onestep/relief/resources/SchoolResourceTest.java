package org.onestep.relief.resources;

import io.dropwizard.testing.junit5.DropwizardExtensionsSupport;
import io.dropwizard.testing.junit5.ResourceExtension;
import org.junit.Ignore;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.onestep.relief.dynamodb.models.School;
import org.onestep.relief.dynamodb.services.SchoolDynamoDbService;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 * Test school resources with in-memory container
 * without starting application
 */
@Disabled
@ExtendWith(DropwizardExtensionsSupport.class)
public class SchoolResourceTest {

    // mock dynamoDB service
    private static final SchoolDynamoDbService schoolDAO = mock(SchoolDynamoDbService.class);

    public static final ResourceExtension RESOURCE = ResourceExtension.builder()
            .addResource(new SchoolCollectionResource(schoolDAO))
            .build();

    // endpoints
    private static final String CREATE = "/schools";
    private static final String GETALL = "/schools";
    private static final String GETONE = "/schools/1";

    private School testSchool;
    private List <School> testSchoolList = new ArrayList<>();

    @BeforeEach
    void setup() {
        // instantiate test school
        testSchool = new School();
        testSchool.setId("1");
        testSchool.setName("Unit Test School");
        testSchool.setStreetAddress("123 Main St");
        testSchool.setCity("Boston");
        testSchool.setStateOrProvince("MA");
        testSchool.setCountry("USA");

        testSchoolList.add(testSchool);
    }

    @AfterEach
    void tearDown() {
        reset(schoolDAO);
    }

    @Test
    public void getSchoolsTest() {
        when(schoolDAO.getAllSchools()).thenReturn(testSchoolList);
        List<School> response = RESOURCE.target(GETALL).request().get(new GenericType<List<School>>(){});
        // check response list the same size as mocked list
        assertThat(response.size()).isEqualTo(testSchoolList.size());
    }

    @Test
    public void deleteSchoolTest(){

        Response response;

        // get exiting school to delete
        when(schoolDAO.findSchoolByID(any(String.class))).thenReturn(testSchool);
        //DELETE request
        response = RESOURCE.target(GETONE).request().delete();
        //successful delete
        assertThat(response.getStatus()).isEqualTo(Response.Status.NO_CONTENT.getStatusCode());

        //trying to delete a null school
        when(schoolDAO.findSchoolByID(any(String.class))).thenReturn(null);
        //DELETE request
        response = RESOURCE.target(GETONE).request().delete();
        //not found response
        assertThat(response.getStatus()).isEqualTo(Response.Status.NOT_FOUND.getStatusCode());;
    }

    @Test
    public void getSchoolTest(){
        when(schoolDAO.findSchoolByID(any(String.class)))
                .thenReturn(testSchool);
        School response = RESOURCE.target(GETONE).request().get(School.class);
        //verify the school id returned
        assertThat(response.getId()).isEqualTo(testSchool.getId());
    }

    @Test
    public void createSchoolTest(){
        Response response = RESOURCE.target(CREATE).request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(testSchool,MediaType.APPLICATION_JSON));
        //check created school id the same as supplied id
        assertThat(response.readEntity(School.class).getId()).isEqualTo(testSchool.getId());
        // check status code response
        assertThat(response.getStatus()).isEqualTo(Response.Status.CREATED.getStatusCode());
    }

    @Test
    public void updateSchoolTest(){
        //verify city name before update
        assertThat(testSchool.getName() == "Boston");

        //sending a city update for a school
        String newCity = "Cambridge";
        School schoolUpdate = new School();
        schoolUpdate.setCity(newCity);

        when(schoolDAO.findSchoolByID(any(String.class)))
                .thenReturn(testSchool);
        //PUT request with updated school value
        Response response = RESOURCE.target(GETONE).request(MediaType.APPLICATION_JSON)
                .put(Entity.entity(schoolUpdate,MediaType.APPLICATION_JSON));
        //value change
        assertThat(response.readEntity(new GenericType<School>(){}).getCity())
                .isEqualTo(newCity);
        //successful update
        assertThat(response.getStatus()).isEqualTo(Response.Status.OK.getStatusCode());


    }

    @Test
    public void updateSchoolIdTest(){
        //current school id
        String originalSchoolId = testSchool.getId();
        assertThat(originalSchoolId == "1");
        //setting a new id for school in request
        School schoolUpdate = new School();
        schoolUpdate.setId("2");

        when(schoolDAO.findSchoolByID(any(String.class)))
                .thenReturn(testSchool);
        //PUT request with updated school id
        Response response = RESOURCE.target(GETONE).request(MediaType.APPLICATION_JSON)
                .put(Entity.entity(schoolUpdate,MediaType.APPLICATION_JSON));
        //check school has not change after put request
        assertThat(response.readEntity(new GenericType<School>(){}).getId())
                .isEqualTo(originalSchoolId);
    }
}
