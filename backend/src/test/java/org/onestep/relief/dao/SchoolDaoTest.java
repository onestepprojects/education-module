package org.onestep.relief.dao;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.model.*;
import org.junit.Ignore;
import org.junit.jupiter.api.*;
import org.onestep.relief.ServiceEnv;
import org.onestep.relief.dynamodb.config.DynamoDBConfig;
import org.onestep.relief.dynamodb.models.School;
import org.onestep.relief.dynamodb.services.SchoolDynamoDbService;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@Disabled
public class SchoolDaoTest {

    private static void createTable() throws Exception {
        AmazonDynamoDB client = null;
        try {
            client = DynamoDBConfig.getInstance().getDBClient();
        }catch (Exception e) {
            System.out.println("Failed to connect to DynamoDB Instance. Check your env properties or start DynamoDB Locally first.");
            e.printStackTrace();
            fail("Failed to connect to DynamoDB");
        }
        try {

            String tableName = "School";
            System.out.println("Creating table in local DB....");
            CreateTableRequest ctr = new CreateTableRequest(tableName,
                    Arrays.asList(new KeySchemaElement("id", KeyType.HASH)))
                    .withAttributeDefinitions(Arrays.asList(new AttributeDefinition("id",
                            ScalarAttributeType.S)))
                    .withProvisionedThroughput(new ProvisionedThroughput(5L,5L));
            CreateTableResult table = client.createTable(ctr);

            System.out.println(table.getTableDescription().toString());
            System.out.println("Done creating table in local DB");

        }catch(Exception e) {
            System.out.println("Error creating table");
            e.printStackTrace();
            fail("Unexpected failure while creating table.");
        }
    }

    @BeforeAll
    public static void setupClass() throws Exception {
        Map<String,String> testEnv = new HashMap<>();
        testEnv.put("DDB_REGION_NAME","us-east-1");
        testEnv.put("AWS_ACCESS_KEY_ID","testKey");
        testEnv.put("AWS_SECRET_ACCESS_KEY","testSecret");
        testEnv.put("DDB_USE_LOCAL","true");
        ServiceEnv.getInstance().initializeEnv(testEnv,true);
        createTable();
    }

    @AfterAll
    public static void tearDownClass() throws Exception{
        AmazonDynamoDB client = DynamoDBConfig.getInstance().getDBClient();
        System.out.println("Deleting table...");
        DeleteTableResult result = client.deleteTable("School");
    }

    @AfterEach
    public void cleanUpTests() throws Exception {
        //Cleanup any residual schools after each test
        SchoolDynamoDbService schoolDB = new SchoolDynamoDbService();
        List<School> allSchools = schoolDB.getAllSchools();
        for(School school: allSchools) {
            schoolDB.deleteSchool(school);
        }
    }

    @Test
    public void testGetSchools() throws Exception {
        SchoolDynamoDbService schoolDB = new SchoolDynamoDbService();
        assertEquals(0,schoolDB.getAllSchools().size());
    }

    @Test
    public void testSaveSchool() throws Exception {
        SchoolDynamoDbService schoolDB = new SchoolDynamoDbService();
        School school = new School();
        school.setCity("Boston");
        school.setName("MIT");
        school.setCountry("USA");
        school.setStateOrProvince("MA");
        schoolDB.saveSchool(school);
        //verify the school was saved by querying.
        List<School> schools = schoolDB.getAllSchools();
        assertEquals(1,schools.size());
        //verify the integrity of the data
        School s = schools.get(0);
        assertEquals("MIT",s.getName());
    }

    @Test
    public void testDeleteSchool() throws Exception {
        SchoolDynamoDbService schoolDB = new SchoolDynamoDbService();
        //first create some schools
        School school = new School();
        school.setName("Harvard");
        school.setStreetAddress("Harvard Square");
        schoolDB.saveSchool(school);
        school = new School();
        school.setName("MIT");
        school.setStreetAddress("Kendal Square");
        school.setStateOrProvince("MA");
        schoolDB.saveSchool(school);
        //verify that 2 objects have been saved
        assertEquals(2,schoolDB.getAllSchools().size());
        String toDeleteID = school.getId();
        School toDeleteSchool = schoolDB.findSchoolByID(toDeleteID);
        assertNotNull(toDeleteSchool);
        schoolDB.deleteSchool(school);
        //Verify that only 1 object remains after delete
        assertEquals(1, schoolDB.getAllSchools().size());
    }

    @Test
    public void testUpdateSchool() throws Exception {
        SchoolDynamoDbService schoolDB = new SchoolDynamoDbService();
        School school = new School();
        school.setName("Tufts");
        school.setStreetAddress("Porter Square");
        schoolDB.saveSchool(school);
        //Verify school was actually saved
        School savedSchool = schoolDB.findSchoolByID(school.getId());
        assertNotNull(savedSchool);
        //update the address and add a state
        savedSchool.setStreetAddress("Davis Square");
        savedSchool.setCountry("USA");
        schoolDB.updateSchool(savedSchool);
        //Verify update by retrieving yet another copy with the same id
        School updatedSchool = schoolDB.findSchoolByID(savedSchool.getId());
        assertNotNull(updatedSchool);
        assertEquals("Davis Square",updatedSchool.getStreetAddress());
        assertEquals("USA",updatedSchool.getCountry());
        assertEquals("Tufts",updatedSchool.getName());

    }

}
