package org.onestep.relief;

import com.codahale.metrics.health.HealthCheck;

public class EducationHealthCheck extends HealthCheck {

  @Override
  protected Result check() throws Exception {
    return Result.healthy();
  }
}
