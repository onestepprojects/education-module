package org.onestep.relief.dynamodb.models;

import com.amazonaws.services.dynamodbv2.datamodeling.*;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Notification entity
 */

public class Person {
  private String id;
  private String name;
  private String email;
  private String role;
  private String schoolId;

  public Person(@JsonProperty("id") String id,
                @JsonProperty("name") String name,
                @JsonProperty("email") String email,
                @JsonProperty("role") String role,
                @JsonProperty("schoolId") String schoolId) {
    this.id = id;
    this.name = name;
    this.email = email;
    this.role = role;
    this.schoolId = schoolId;
  }

  @JsonProperty
  public String getId() {
    return id;
  }

  @JsonProperty
  public void setId(String id) {
    this.id = id;
  }

  @JsonProperty
  public String getName() {
    return name;
  }

  @JsonProperty
  public void setName(String name) {
    this.name = name;
  }

  @JsonProperty
  public String getEmail() {
    return email;
  }

  @JsonProperty
  public void setEmail(String email) {
    this.email = email;
  }

  @JsonProperty
  public String getRole() {
    return role;
  }

  @JsonProperty
  public void setRole(String role) {
    this.role = role;
  }

  @JsonProperty
  public String getSchoolId() {
    return schoolId;
  }

  @JsonProperty
  public void setSchoolId(String schoolId) {
    this.schoolId = schoolId;
  }

  @java.lang.Override
  public java.lang.String toString() {
    return "Person{" +
            "id='" + id + '\'' +
            ", name='" + name + '\'' +
            ", email='" + email + '\'' +
            ", role='" + role + '\'' +
            ", schoolId='" + schoolId + '\'' +
            '}';
  }
}
