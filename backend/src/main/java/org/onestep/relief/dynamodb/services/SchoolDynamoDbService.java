package org.onestep.relief.dynamodb.services;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.google.inject.Inject;
import org.onestep.relief.dynamodb.config.DynamoDBConfig;
import org.onestep.relief.dynamodb.models.School;

import java.util.List;
import java.util.UUID;

public class SchoolDynamoDbService {

    private final DynamoDBMapper mapper;

    @Inject
    public SchoolDynamoDbService() {
        this.mapper = DynamoDBConfig.getInstance().getDynamoDBMapper();
    }

    // get all school items
    public List<School> getAllSchools() {
        return this.mapper.scan(School.class, new DynamoDBScanExpression());
    }

    public void saveSchool(School school) {
        //always need an ID. If it's null then create it
        if(school.getId() == null) {
            school.setId(UUID.randomUUID().toString());
        }
        mapper.save(school);
    }

    public void updateSchool(School school) {
        mapper.save(school);
    }

    public void deleteSchool(School school) {
        mapper.delete(school);
    }

    public School findSchoolByID(String id) {
        return mapper.load(School.class,id);
    }
}
