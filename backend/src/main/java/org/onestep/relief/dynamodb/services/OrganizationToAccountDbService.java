package org.onestep.relief.dynamodb.services;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import org.onestep.relief.dynamodb.config.DynamoDBConfig;
import org.onestep.relief.dynamodb.models.OrganizationToAccount;

import java.util.List;

public class OrganizationToAccountDbService {
    private final DynamoDBMapper mapper;

    public OrganizationToAccountDbService() {
        this.mapper = DynamoDBConfig.getInstance().getDynamoDBMapper();
    }

    public OrganizationToAccount findMappingByOrgId(String orgId) {
        return mapper.load(OrganizationToAccount.class,orgId);
    }

    public void saveMapping(OrganizationToAccount mapping) {
        mapper.save(mapping);
    }

    public List<OrganizationToAccount> getAllMappings() {
        return mapper.scan(OrganizationToAccount.class,new DynamoDBScanExpression());
    }

    public void deleteMapping(String orgId) {
        OrganizationToAccount organizationToAccount = mapper.load(OrganizationToAccount.class,orgId);

        mapper.delete(organizationToAccount);
    }

}
