package org.onestep.relief.dynamodb.models;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIndexHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

@DynamoDBTable(tableName = "OrgToAccountMap")
public class OrganizationToAccount {

    @DynamoDBAttribute(attributeName = "id")
    @DynamoDBHashKey
    private String organizationId;

    private String accountId;

    private String schoolName;

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getSchoolName() { return schoolName;}

    public void setSchoolName(String schoolName) { this.schoolName = schoolName; }

}
