package org.onestep.relief.dynamodb.models;


import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

@DynamoDBTable(tableName = "OrgUserToCanvasUserMap")
public class OrgUserToCanvasUser {

    @DynamoDBAttribute(attributeName = "orgUserId")
    @DynamoDBHashKey
    private String orgUserId;

    private String canvasUserId;

    public String getOrgUserId() {
        return orgUserId;
    }

    public void setOrgUserId(String orgUserId) {
        this.orgUserId = orgUserId;
    }

    public String getCanvasUserId() {
        return canvasUserId;
    }

    public void setCanvasUserId(String canvasUserId) {
        this.canvasUserId = canvasUserId;
    }
}
