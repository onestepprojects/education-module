package org.onestep.relief.dynamodb.services;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import org.onestep.relief.dynamodb.config.DynamoDBConfig;
import org.onestep.relief.dynamodb.models.OrgUserToCanvasUser;
import org.onestep.relief.dynamodb.models.OrganizationToAccount;

import java.util.List;

/**
 * This is the DynamoDB helper service
 * to deal with Org user to Canvas user mapping
 */
public class OrgUserToCanvasUserDBService {


    private final DynamoDBMapper mapper;

    public OrgUserToCanvasUserDBService() {
        this.mapper = DynamoDBConfig.getInstance().getDynamoDBMapper();
    }

    public OrgUserToCanvasUser findCanvasUserByOrgUser(String orgUserId) {
        return mapper.load(OrgUserToCanvasUser.class, orgUserId);
    }

    public void saveMapping(OrgUserToCanvasUser mapping) {
        mapper.save(mapping);
    }

    public List<OrgUserToCanvasUser> getAllMappings() {
        return mapper.scan(OrgUserToCanvasUser.class,new DynamoDBScanExpression());
    }
}
