package org.onestep.relief.dynamodb.services;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.google.inject.Inject;
import org.onestep.relief.dynamodb.config.DynamoDBConfig;
import org.onestep.relief.dynamodb.models.CanvasRolePermission;

import java.util.List;

/**
 * Service to interact with dynamoDb CanvasRolePermission table
 */
public class CanvasRolePermissionDbService {

    private final DynamoDBMapper mapper;

    /**
     * Inject dynamoDb client configuration
     */
    @Inject
    public CanvasRolePermissionDbService(){
        this.mapper = DynamoDBConfig.getInstance().getDynamoDBMapper();
    }

    /**
     * Retrieve all roles permissions from database
     * @return list of roles
     */
    public List<CanvasRolePermission> getAllPermissions() {
        return this.mapper.scan(CanvasRolePermission.class, new DynamoDBScanExpression());
    }

    /**
     * Retrieve all permissions from database for a given role
     *
     * @param roleId: role to get permission from (dyanmoDb hash key)
     * @return list fo permission for the role found in dynamoDb
     */
    public List<CanvasRolePermission> getAllPermissionsByRole(String roleId){

        DynamoDBQueryExpression<CanvasRolePermission> dbQueryExpression = new DynamoDBQueryExpression<>();
        // construct object to retrieve base on role id
        CanvasRolePermission hashKeyItems = new CanvasRolePermission();
        hashKeyItems.setRoleId(roleId);
        dbQueryExpression.setHashKeyValues(hashKeyItems);

        return  this.mapper.query( CanvasRolePermission.class, dbQueryExpression);

    };
}
