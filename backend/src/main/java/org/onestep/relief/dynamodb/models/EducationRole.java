package org.onestep.relief.dynamodb.models;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

/**
 * Represent Education Module roles defined in database table
 */
@DynamoDBTable(tableName = "EducationRole")
public class EducationRole {

    @DynamoDBHashKey
    private String roleId;

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getRoleId() {
        return roleId;
    }
}
