package org.onestep.relief.dynamodb.models;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

/**
 * Canvas Role Permission entity
 */
@DynamoDBTable(tableName = "CanvasRolePermission")
public class CanvasRolePermission {

    private String roleId;
    private String permissionParameter;
    private String permissionValue;

    @DynamoDBHashKey
    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    @DynamoDBAttribute
    public String getPermissionParameter() {
        return permissionParameter;
    }

    public void setPermissionParameter(String permissionParameter) {
        this.permissionParameter = permissionParameter;
    }

    @DynamoDBAttribute
    public String getPermissionValue() {
        return permissionValue;
    }

    public void setPermissionValue(String parameterValue) {
        this.permissionValue = parameterValue;
    }
}

