package org.onestep.relief.dynamodb.models;

import com.amazonaws.services.dynamodbv2.datamodeling.*;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * School entity
 */

@DynamoDBTable(tableName = "School")
public class School   {

  private String id;

  private String name;

  private String streetAddress;

  private String city;

  private String stateOrProvince;

  private String country;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  @DynamoDBGeneratedUuid(DynamoDBAutoGenerateStrategy.CREATE)
  @DynamoDBHashKey
  public String getId() {
    return id;
  }

  public School setId(String id) {
    this.id = id;
    return this;
  }

  @DynamoDBAttribute
  public String getName() {
    return name;
  }

  public School setName(String name) {
    this.name = name;
    return this;
  }

  @DynamoDBAttribute
  public String getStreetAddress() {
    return streetAddress;
  }

  public School setStreetAddress(String streetAddress) {
    this.streetAddress = streetAddress;
    return this;
  }

  @DynamoDBAttribute
  public String getCity() {
    return city;
  }

  public School setCity(String city) {
    this.city = city;
    return this;
  }

  @DynamoDBAttribute(attributeName = "state")
  public String getStateOrProvince() {
    return stateOrProvince;
  }

  public School setStateOrProvince(String stateOrProvince) {
    this.stateOrProvince = stateOrProvince;
    return this;
  }

  @DynamoDBAttribute
  public String getCountry() {
    return country;
  }

  public School setCountry(String country) {
    this.country = country;
    return this;
  }

}
