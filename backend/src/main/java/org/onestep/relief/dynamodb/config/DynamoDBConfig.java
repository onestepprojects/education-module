package org.onestep.relief.dynamodb.config;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.internal.StaticCredentialsProvider;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import org.onestep.relief.ServiceEnv;

/**
 * Create Dynamodb client connection and DynamoDBMapper class
 */
public class DynamoDBConfig {
    private final static ServiceEnv ENV = ServiceEnv.getInstance();
    private final static AmazonDynamoDB client;



    static {
        AmazonDynamoDBClientBuilder builder = AmazonDynamoDBClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(
                        new BasicAWSCredentials(ENV.getEnv("AWS_ACCESS_KEY_ID"),
                                ENV.getEnv("AWS_SECRET_ACCESS_KEY"))));

        boolean useLocalDB = Boolean.parseBoolean(ENV.getEnv("DDB_USE_LOCAL","false"));
        if(useLocalDB) {
            builder.withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(
                    String.format("http://%s:%s",ENV.getEnv("DDB_HOST","localhost"),
                            ENV.getEnv("DDB_PORT","8000")),ENV.getEnv("DDB_REGION_NAME")));
        }else {
            builder.withRegion(ENV.getEnv("DDB_REGION_NAME"));
        }
        client = builder.build();
    }

    private static DynamoDBConfig instance = new DynamoDBConfig();

    private DynamoDBConfig() {

    }

    public static DynamoDBConfig getInstance() {
        return instance;
    }

    public DynamoDBMapper getDynamoDBMapper() {
        DynamoDBMapper mapper = new DynamoDBMapper(client);
        return mapper;
    }

    public AmazonDynamoDB getDBClient() {
        return client;
    }
}
