package org.onestep.relief.dynamodb.services;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.google.inject.Inject;
import org.onestep.relief.dynamodb.config.DynamoDBConfig;
import org.onestep.relief.dynamodb.models.EducationRole;

import java.util.List;

/**
 * Service to interact with dynamoDb EducationRole table
 */
public class RoleDynamoDbService {

    private final DynamoDBMapper mapper;

    /**
     * Inject dynamoDb client configuration
     */
    @Inject
    public RoleDynamoDbService() {
        this.mapper = DynamoDBConfig.getInstance().getDynamoDBMapper();
    }

    /**
     * Retrieve all roles from database
     * @return list of roles
     */
    public List<EducationRole> getRoles() {
        return this.mapper.scan(EducationRole.class, new DynamoDBScanExpression());
    }

}
