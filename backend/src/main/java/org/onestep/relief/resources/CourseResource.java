package org.onestep.relief.resources;


import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.AbstractMap;
import javax.ws.rs.*;
import javax.ws.rs.client.Client;
import javax.ws.rs.core.MediaType;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import org.onestep.relief.client.CanvasClient;

@OpenAPIDefinition(
        info = @Info(
                title="Education APIs",
                version = "1.0.1"
        )
)
@Path("courses")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class CourseResource {

  private CanvasClient canvasClient;

  public CourseResource(Client client) {
    this.canvasClient = new CanvasClient(client);
  }


  @GET
  @Operation(summary = "Provides a list of all courses in the Education Module (for Global Admins)")
  public String getAllCourses() {
    return canvasClient.getAllCourses();
  }

  @GET
  @Path("/account/{id}")
  @Operation(summary = "Provides a list of all courses for a given school (for School Admins/Teachers")
  public String getAllCourses(@PathParam("id") String accountId) {
    return canvasClient.getCoursesForAccount(accountId);
  }

  @GET
  @Path("/user/{id}")
  @Operation(summary = "Displays courses based on user ID (for Teachers/Students")
  public String getCoursesForUser(@PathParam("id") String userId) {
    return canvasClient.getCoursesForUser(userId);
  }
}