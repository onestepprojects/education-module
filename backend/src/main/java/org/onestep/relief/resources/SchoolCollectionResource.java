package org.onestep.relief.resources;


import java.util.*;
import java.util.stream.Collectors;
import javax.ws.rs.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.client.Client;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import org.onestep.relief.client.CanvasClient;
import org.onestep.relief.client.OrganizationClient;
import org.onestep.relief.dynamodb.models.*;
import org.onestep.relief.dynamodb.services.OrgUserToCanvasUserDBService;
import org.onestep.relief.dynamodb.services.OrganizationToAccountDbService;
import org.onestep.relief.dynamodb.services.SchoolDynamoDbService;
import org.onestep.relief.representations.*;
import org.yaml.snakeyaml.tokens.ScalarToken;


@OpenAPIDefinition(
        info = @Info(
                title="Education APIs",
                version = "1.0.1"
        )
)
@Path("schools")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class SchoolCollectionResource {

  private OrganizationToAccountDbService orgMappingDBService;

  private OrganizationClient organizationClient;

  private CanvasClient canvasClient;

  private OrgUserToCanvasUserDBService orgUserToCanvasUserDBService;

  @Context
  private javax.ws.rs.container.ResourceContext rc;

  public SchoolCollectionResource(Client client) {
    this.orgMappingDBService = new OrganizationToAccountDbService();
    this.organizationClient = new OrganizationClient(client);
    this.canvasClient = new CanvasClient(client);
    this.orgUserToCanvasUserDBService = new OrgUserToCanvasUserDBService();
  }

  // Unit Test constructor
  public SchoolCollectionResource(SchoolDynamoDbService service){

  }

  @GET
  @Operation(summary = "Provides a list of registered schools")
  public List<SchoolRepresentation> getSchools() {
    List<SchoolRepresentation> allSchools = organizationClient.getEducationalOrgs();
    final List<OrganizationToAccount> mappedSchools = orgMappingDBService.getAllMappings();
    final Set<String> mappedIds = mappedSchools.stream().map(o -> o.getOrganizationId()).collect(Collectors.toSet());
    return allSchools.stream().filter(s -> mappedIds.contains(s.getId())).collect(Collectors.toList());
  }

  @DELETE
  @Path("/{id}")
  @Operation(summary = "Deletes school using the school ID")
  public Response deleteSchool(@PathParam("id") String schoolId,
                               @HeaderParam("Authorization") Optional<String> authToken) {

    //Try to delete Canvas account
    OrganizationToAccount organizationToAccount = orgMappingDBService.findMappingByOrgId(schoolId);
    String accountId = organizationToAccount.getAccountId();
    System.out.println("Canvas accountId: " + accountId);
    try {
      canvasClient.deleteCanvasAccount(accountId);
    } catch (Exception ex) {
      return Response.status(409).entity("You can't delete a school that has courses and/or sub-accounts in it").build();
    }

    //Delete school in Org Module
    organizationClient.deleteSchool(schoolId,authToken);

    //Finally delete the mapping
    orgMappingDBService.deleteMapping(schoolId);

    return Response.noContent().build();
  }

  @GET
  @Path("/{id}")
  @Operation(summary = "Displays school based on the school ID")
  public SchoolRepresentation getSchool(@PathParam("id") String schoolId) {
    return organizationClient.getSchoolById(schoolId);
  }


  @POST
  @Operation(summary = "Creates a new school")
  public SchoolRepresentation createSchool(@NotNull @Valid SchoolRepresentation schoolRequest,
                               @HeaderParam("Authorization") Optional<String> authToken) {
    System.out.println("Creating new school " + schoolRequest.getName());
    //First create Organization
    SchoolRepresentation school = organizationClient.createSchool(schoolRequest,authToken);
    System.out.println("Crated Organization " + school.getId());
    //Now create an account in canvas
    CanvasAccount account = canvasClient.createCanvasAccount(schoolRequest.getName());
    System.out.println("Created Canvas account " + account.getId());
    //Add roles to account in canvas
    canvasClient.addCanvasAccountRoles(account);

    //Now store a mapping of account->org
    OrganizationToAccount organizationToAccount = new OrganizationToAccount();
    organizationToAccount.setOrganizationId(school.getId());
    organizationToAccount.setAccountId(account.getId());
    organizationToAccount.setSchoolName(schoolRequest.getName());
    orgMappingDBService.saveMapping(organizationToAccount);
    return school;
  }


  @PUT
  @Path("/{id}")
  @Operation(summary = "Updates school based on the school ID")
  public Response updateSchool(@PathParam("id") String id, @HeaderParam("Authorization")Optional<String> authToken, SchoolRepresentation schoolRequest){

   //First get a mapping
    OrganizationToAccount mapping = orgMappingDBService.findMappingByOrgId(id);
    if(mapping == null) {
      //No mapping found so return 404 NOT FOUND
      throw new NotFoundException("No School with id " + id + " found mapped to an Organization.");
    }
    SchoolRepresentation rep = organizationClient.updateSchool(schoolRequest,authToken);
    return Response.ok().entity(rep).build();
  }

  /**
   * Associates a person to a school in the Organization Module.
   * Creates a new user account in Canvas. Stores the mapping in
   * dynamoDB.
   *
   * @param schoolId    -   school id
   * @param userId      -   user id
   * @param role        -   role for the user
   * @param userName    -   user name
   * @param email       -   user's email id
   * @param authToken   -   authorization token
   */
  @POST
  @Path("/registerUser/{schoolId}/{userId}/{role}/{userName}/{email}")
  @Operation(summary = "Associates role to user in Org module, creates new user in canvas, and assigns roles to user in canvas.")
  public void registerUser(
          @PathParam("schoolId") String schoolId,
          @PathParam("userId") String userId,
          @PathParam("role") String role,
          @PathParam("userName") String userName,
          @PathParam("email") String email,
          @HeaderParam("Authorization") Optional<String> authToken) {
    //TODO validate userId and schoolId are valid

    // Step1: register userId with schoolId in Org
    organizationClient.associatePersonToSchool(userId, schoolId, role, authToken);

    // Step2: get canvas accountId for the schoolId
    OrganizationToAccount mapping = orgMappingDBService.findMappingByOrgId(schoolId);

    String accountId = mapping.getAccountId();
    System.out.println("Registered user's AccountId: " + accountId);

    // Step3: create userId with canvas accountId
    String canvasUserId = canvasClient.createUser(accountId, userId, userName, email);

    // Step4: assign roles to user
    canvasClient.assignAccountRoleToUser(accountId, canvasUserId, role);
    System.out.println("Assigned roles: " + role + " to canvas user id :" + userId);

    // mapping of orgUserId -> canvasUerId
    OrgUserToCanvasUser orgUserToCanvasUser = new OrgUserToCanvasUser();
    orgUserToCanvasUser.setOrgUserId(userId);
    orgUserToCanvasUser.setCanvasUserId(canvasUserId);
    orgUserToCanvasUserDBService.saveMapping(orgUserToCanvasUser);

  }

  @GET
  @Path("/canvasUserId/{orgUserId}")
  @Operation(summary= "Gets a user's Canvas id based on the user's Org id.")
  public String getCanvasUserId(@PathParam("orgUserId") String orgUserId) {
      OrgUserToCanvasUser mapping = orgUserToCanvasUserDBService.findCanvasUserByOrgUser(orgUserId);
      return mapping.getCanvasUserId();
  }

  @GET
  @Path("/canvasAccountId/{orgId}")
  @Operation(summary = "Gets Account Id based on Org Id.")
  public String getAccountId(@PathParam("orgId") String orgId) {
      OrganizationToAccount mapping = orgMappingDBService.findMappingByOrgId(orgId);
      return mapping.getAccountId();
  }

  @GET
  @Path("/users/{userid}")
  @Operation(summary = "Gets a user's role and associated school(s).")
  public RolesRepresentation[] getUser(@PathParam("userid") String userid) {
    return organizationClient.getOrgRoleByPersonId(userid);
  }


  private List<PersonRepresentation> getRegisteredUsers(Optional<String> authToken) {
    final String allRoles[] = {"STUDENT","TEACHER","ADMIN","GLOBAL_ADMIN"};
    Set<PersonRepresentation> users = new HashSet<>();

    List<OrganizationToAccount> allMappings = orgMappingDBService.getAllMappings();
    for(OrganizationToAccount mapping : allMappings) {
      String orgId = mapping.getOrganizationId();
      try {
        List<PersonRepresentation> orgUsers = organizationClient.getPersonsForOrg(orgId,authToken);
        List<PersonRepresentation> filteredUsers = orgUsers.stream().filter(p -> p.hasAnyRole(allRoles) ).collect(Collectors.toList());
        filteredUsers.forEach(p -> {p.setSchool(mapping.getSchoolName());});
        users.addAll(filteredUsers);
      }catch(Exception e) {
        e.printStackTrace();
      }

    }
    return users.stream().collect(Collectors.toList());
  }
  @GET
  @Path("/users")
  @Operation(summary = "Gets users and roles for each school. This query can be for registered users (Users already associated with a school) or unregistered users (not associated with a school)")
  public List<PersonRepresentation> getUsers(@QueryParam("registered") Boolean isRegistered, @HeaderParam("Authorization") Optional<String> authToken) {
    boolean reg = (isRegistered != null) ? isRegistered: false;
    if(reg) {
      return getRegisteredUsers(authToken);
    }else {
      //First get registered users
      List<PersonRepresentation> registered = getRegisteredUsers(authToken);
      List<PersonRepresentation> allPersons = organizationClient.getPersons();
      return allPersons.stream().filter(p -> !registered.contains(p)).collect(Collectors.toList());
    }

  }
}