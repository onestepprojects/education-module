package org.onestep.relief.utils;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.onestep.relief.client.CanvasClient;
import org.onestep.relief.client.OrganizationClient;
import org.onestep.relief.dynamodb.models.OrganizationToAccount;
import org.onestep.relief.dynamodb.services.OrganizationToAccountDbService;
import org.onestep.relief.representations.SchoolRepresentation;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import java.util.List;

public class OrgToCanvasMapUtil {

    public static void main(String args[]) throws Exception {
        OrganizationToAccountDbService orgMappingDBService = new OrganizationToAccountDbService();
        List<OrganizationToAccount> allMappings = orgMappingDBService.getAllMappings();
        System.out.println(String.format("Syncing %d Organizations",allMappings.size()));
        ClientConfig config = new ClientConfig().property(ClientProperties.FOLLOW_REDIRECTS,true);
        Client client = ClientBuilder.newClient(config);
        OrganizationClient orgClient = new OrganizationClient(client);
        CanvasClient canvasClient = new CanvasClient(client);
        for(OrganizationToAccount ota: allMappings) {
            String orgId = ota.getOrganizationId();
            try {
                SchoolRepresentation school = orgClient.getSchoolById(orgId);
                System.out.println("Updating " + school.getName());
                ota.setSchoolName(school.getName());
                orgMappingDBService.saveMapping(ota);
            }catch (Exception e) {
                System.out.println("Failed to update school " + orgId);
                try {
                    String courses = canvasClient.getCoursesForAccount(ota.getAccountId());
                    System.out.println("Got courses for failed School:\n" + courses);
                }catch (Exception ex) {
                    System.out.println("Failed creating Canvas account " + ota.getAccountId());
                    orgMappingDBService.deleteMapping(orgId);
                }
            }

        }
    }
}
