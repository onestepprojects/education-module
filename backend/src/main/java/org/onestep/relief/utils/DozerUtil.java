package org.onestep.relief.utils;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.dozer.loader.api.BeanMappingBuilder;
import static org.dozer.loader.api.TypeMappingOptions.*;
import org.onestep.relief.dynamodb.models.School;

/**
 * Utility class for Dozer configuration
 * Dozer is a Java Bean to Java Bean mapper that recursively copies data from one object to another.
 *
 * doc ref: http://dozer.sourceforge.net/documentation/about.html
 */
public class DozerUtil {

    /**
     * return  mapper instance to be used for mapping
     *
     * @return
     */
    public static Mapper getMapper() {
        DozerBeanMapper mapper = new DozerBeanMapper();
        mapper.addMapping(beanMappingBuilder());
        return mapper;
    }

    /**
     * Configure mapper to ignore null and missing values
     * when mapping source object to destination object.
     *
     * ref: http://dozer.sourceforge.net/documentation/apimappings.html
     *
     * @return BeanMappingBuilder
     */
    private static BeanMappingBuilder beanMappingBuilder() {
        return new BeanMappingBuilder() {
            @Override
            protected void configure() {
                //Mapping school
                //TODO: try mapping Object.class instead
                // to reuse for other entities
                mapping(School.class, School.class, mapNull(false)
                        , mapEmptyString(false)).exclude("id");
            }
        };
    }

}

