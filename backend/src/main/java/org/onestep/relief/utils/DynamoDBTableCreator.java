package org.onestep.relief.utils;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.model.*;
import org.onestep.relief.dynamodb.config.DynamoDBConfig;

import java.util.Arrays;

public class DynamoDBTableCreator {

    public static void main(String[] args) {
        AmazonDynamoDB client = null;
        try {
            client = DynamoDBConfig.getInstance().getDBClient();
            String tableName = "OrgToAccountMap";
            System.out.println("Creating table...");
            CreateTableRequest ctr = new CreateTableRequest(tableName,
                    Arrays.asList(new KeySchemaElement("id", KeyType.HASH)))
                    .withAttributeDefinitions(Arrays.asList(new AttributeDefinition("id",
                            ScalarAttributeType.S)))
                    .withProvisionedThroughput(new ProvisionedThroughput(5L,5L));
            CreateTableResult table = client.createTable(ctr);

            System.out.println(table.getTableDescription().toString());
        }catch (Exception e) {
            System.out.println("Failed to connect to DynamoDB Instance. Check your env properties or start DynamoDB Locally first.");
            e.printStackTrace();
        }
    }
}
