package org.onestep.relief.client;

import org.onestep.relief.ServiceEnv;
import org.onestep.relief.representations.PersonRepresentation;
import org.onestep.relief.representations.RolesRepresentation;
import org.onestep.relief.representations.SchoolRepresentation;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

public class OrganizationClient {

    private static final Logger LOGGER = Logger.getLogger(OrganizationClient.class.getName());
    private static final String ORGANIZATIONS_BASE_PATH = "organizations";

    private static final String PERSONS_BASE_PATH = "persons";

    private static final String RELATIONS_BASE_PATH = "composite";

    private String organizationBaseURL;

    private String personsBaseURL;

    private String relationsBaseURL;

    private Client serviceClient;

    public OrganizationClient(Client client) {
        serviceClient = client;
        ServiceEnv env = ServiceEnv.getInstance();
        String oppServiceHost = env.getEnv("OPP_HOSTNAME","localhost");
        String oppPort = env.getEnv("OPP_PORT",null);
        String oppServiceProtocol = env.getEnv("OPP_PROTOCOL","http");

        if(oppPort != null) {
            organizationBaseURL = String.format("%s://%s:%s/%s",
                    oppServiceProtocol,
                    oppServiceHost,
                    oppPort,
                    ORGANIZATIONS_BASE_PATH);
            personsBaseURL = String.format("%s://%s:%s/%s",
                    oppServiceProtocol,
                    oppServiceHost,
                    oppPort,
                    PERSONS_BASE_PATH);
            relationsBaseURL = String.format("%s://%s:%s/%s",
                    oppServiceProtocol,
                    oppServiceHost,
                    oppPort,
                    RELATIONS_BASE_PATH);
        }else {
            LOGGER.info("No port specified for the OPP Module. URL will be constructed without a port number.");
            organizationBaseURL = String.format("%s://%s/%s",
                    oppServiceProtocol,
                    oppServiceHost,
                    ORGANIZATIONS_BASE_PATH);
            personsBaseURL = String.format("%s://%s/%s",
                    oppServiceProtocol,
                    oppServiceHost,
                    PERSONS_BASE_PATH);
            relationsBaseURL = String.format("%s://%s/%s",
                    oppServiceProtocol,
                    oppServiceHost,
                    RELATIONS_BASE_PATH);
        }




    }

    public List<SchoolRepresentation> getEducationalOrgs() {
        WebTarget webTarget = serviceClient.target(organizationBaseURL);
        Response response = webTarget.request(MediaType.APPLICATION_JSON).get();
        if(response.getStatus() == Response.Status.OK.getStatusCode()) {
            return response.readEntity(new GenericType<List<SchoolRepresentation>>(){});
        }
        return new ArrayList<>();
    }

    public SchoolRepresentation getSchoolById(String id) {
        String url = organizationBaseURL + "/" + id;
        SchoolRepresentation rep = callOrganizationService(url, HttpMethod.GET,
                SchoolRepresentation.class,null,Optional.empty());
        WebTarget webTarget = serviceClient.target(organizationBaseURL + "/" + id);
        Response response = webTarget.request(MediaType.APPLICATION_JSON).get();
        if(response.getStatus() == Response.Status.OK.getStatusCode()) {
            return response.readEntity(SchoolRepresentation.class);
        }else {
            throw new NotFoundException();
        }
    }

    public RolesRepresentation[] getOrgRoleByPersonId(String personId) {
        String url = organizationBaseURL + "/get-org-roles-by-person/" + personId;
        RolesRepresentation[] rolesRepresentation = callOrganizationService(url, HttpMethod.GET,
                RolesRepresentation[].class,null,Optional.empty());
        //String roles = callOrganizationService(url, HttpMethod.GET, String.class,null,Optional.empty());
        return rolesRepresentation;
    }

    public RolesRepresentation[] getRoleRequestsByParent(String orgId, Optional<String> authToken) {
        String url = relationsBaseURL + "/role-requests/" + orgId;
        RolesRepresentation[] rolesRepresentation = callOrganizationService(url, HttpMethod.GET,
                RolesRepresentation[].class,null,Optional.empty());
        //String roles = callOrganizationService(url, HttpMethod.GET, String.class,null,Optional.empty());
        return rolesRepresentation;
    }

    public SchoolRepresentation createSchool(SchoolRepresentation rep, Optional<String> authToken) {
        rep.setType("EDUCATIONAL");
        rep.setStatusLabel("ACTIVE");
        rep.setStatusLastActive("" + System.currentTimeMillis());
        WebTarget webTarget = serviceClient.target(organizationBaseURL + "/create");
        String token = authToken.orElse("");
        System.out.println("Token: " + token);
        Response response = webTarget.request(MediaType.APPLICATION_JSON).header("Authorization",token)
                .post(Entity.json(rep));
        if(response.getStatus() == Response.Status.OK.getStatusCode()) {
            return response.readEntity(SchoolRepresentation.class);
        }else {
            System.out.println("Bad request " + response.getStatus());
            String data = response.readEntity(String.class);
            System.out.println(data);
            throw new BadRequestException();
        }
    }

    public void deleteSchool(String schoolId, Optional<String> authToken) {
        String url = organizationBaseURL + "/delete/" + schoolId;
        callOrganizationService(url,HttpMethod.DELETE,Void.class,null,authToken);
    }

    public SchoolRepresentation updateSchool(SchoolRepresentation toUpdate, Optional<String> authToken) {
        String url = organizationBaseURL + "/update/" + toUpdate.getId();
        return callOrganizationService(url,HttpMethod.POST,SchoolRepresentation.class,toUpdate,authToken);
    }

    public void associatePersonToSchool(String userId, String schoolId, String role, Optional<String> authToken) {
        // https://gitlab.com/onestepprojects/organization-module/-/blob/main/backend/src/main/java/org/onestep/relief/dropwizard/resources/RelationResources.java#L51
        String url = relationsBaseURL + "/add-role-to-entity/" + userId + "/" + role + "/organization/" + schoolId;
        callOrganizationService(url, HttpMethod.GET, Void.class, null, authToken);

    }

    public void updateStatusToActive(String role, String orgId, String userId, String status, Optional<String> authToken) {
        //update-person-role-status/{role}/{parentEntityId}/{personId}/{requestStatus}
        String url = relationsBaseURL + "/update-person-role-status/" + role + "/" + orgId + "/" + userId + "/" + status;

        System.out.println("URL: " + url);
        callOrganizationService(url, HttpMethod.GET,Void.class, null, authToken);

    }

    public List<PersonRepresentation> getPersonsForOrg(String orgId, Optional<String> authToken) {
        String url = personsBaseURL + "/get-by-parent/" + orgId;
        PersonRepresentation[] persons = callOrganizationService(url,HttpMethod.GET,PersonRepresentation[].class,null,authToken);
        return Arrays.asList(persons);
    }

    public List<PersonRepresentation> getPersons() {
        WebTarget webTarget = serviceClient.target(personsBaseURL);
        Response response = webTarget.request(MediaType.APPLICATION_JSON).get();
        if(response.getStatus() == Response.Status.OK.getStatusCode()) {
            return response.readEntity(new GenericType<List<PersonRepresentation>>(){});
        }
        return new ArrayList<>();
    }

    public <T> T callOrganizationService(String callURL,
                                         String method,
                                         Class<T> expectedResponseType,
                                         Object entity,
                                         Optional<String> authToken) {
        WebTarget target = serviceClient.target(callURL);
        String token = authToken.orElse("UNKNOWN");
        if(!token.equals("UNKNOWN") && !token.startsWith("Bearer ")) {
            token += "Bearer ";
        }
        Response response = target.request(MediaType.APPLICATION_JSON).header("Authorization",token)
                .method(method,(entity != null) ? Entity.json(entity) : null);
        if(response.getStatus() == Response.Status.OK.getStatusCode()) {
            if(expectedResponseType.equals(Void.class)) {
                return null;
            }else {
                return response.readEntity(expectedResponseType);
            }
        } else {
            String responseStr = response.readEntity(String.class);
            System.out.println("Got error response " + responseStr);
            ClientUtils.throwClientException(response);
            return null;
        }
    }

}
