package org.onestep.relief.client.constants;

/**
 * Query parameter keys for canvas role request
 */
public class CanvasRequestParameterConstant {
    public static final String QUERY_PARAM_ROLE_LABEL = "label";
    public static final  String QUERY_PARAM_BASE_ROLE_TYPE ="base_role_type";

    /* Parameters for account role assignment */
    public static final String QUERY_PARAM_ADMIN_USERID = "user_id";
    public static final String QUERY_PARAM_ADMIN_ROLEID = "role_id";
    public static final String QUERY_PARAM_ADMIN_SENDCONFIRMATION = "send_confirmation";

    /* Parameters for create user request */
    public static final String QUERY_PARAM_USER_NAME = "user[name]";
    public static final String QUERY_PARAM_USER_UNIQUE_ID = "pseudonym[unique_id]";
    public static final String QUERY_PARAM_USER_SIS_ID = "pseudonym[sis_user_id]";
    public static final String QUERY_PARAM_USER_PASSWORD = "pseudonym[password]";
    public static final String QUERY_PARAM_USER_PASSWORD_DEFAULT = "password";


}
