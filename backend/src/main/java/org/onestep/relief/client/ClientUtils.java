package org.onestep.relief.client;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

public class ClientUtils {
    public static void throwClientException(Response response) {
        switch (response.getStatus()) {
            case 404:
                throw new NotFoundException();
            case 400:
                throw new BadRequestException();
            case 401:
            case 403:
                throw new NotAuthorizedException(response);
            case 415:
                throw new NotAcceptableException();
            default:
                throw new ClientErrorException(response.getStatus());

        }
    }
}
