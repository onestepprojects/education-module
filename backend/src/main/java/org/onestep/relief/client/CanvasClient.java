package org.onestep.relief.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.onestep.relief.ServiceEnv;
import org.onestep.relief.client.constants.CanvasRequestParameterConstant;
import org.onestep.relief.dynamodb.models.CanvasRolePermission;
import org.onestep.relief.dynamodb.models.EducationRole;
import org.onestep.relief.dynamodb.services.CanvasRolePermissionDbService;
import org.onestep.relief.dynamodb.services.RoleDynamoDbService;
import org.onestep.relief.representations.CanvasAccount;
import org.onestep.relief.representations.CanvasRole;
import org.onestep.relief.representations.CanvasUser;

import javax.ws.rs.HttpMethod;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.HttpHeaders;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CanvasClient {

    private static final String ACCOUNT_BASE_PATH = "accounts/";
    private static final String USER_BASE_PATH = "users/";
    private static final String COURSE_BASE_PATH = "courses/";
    private static final String ROLE_BASE_PATH = "/roles";
    private static final String ROLE_ASSIGNMENT_BASE_PATH = "/admins";
    private static final String CANVAS_URL_PROPERTY = "CANVAS_URL";
    private static final String CANVAS_API_TOKEN_PROPERTY = "CANVAS_API_TOKEN";
    private static final String CANVAS_ROOT_ACCOUNT_PROPERTY = "CANVAS_ROOT_ACCOUNT";
    private static final String CANVAS_ROOT_ACCOUNT_ID = "1";

    private String canvasBaseURL;
    private Client serviceClient;
    private ServiceEnv env;
    private String rootAccountId;

    private RoleDynamoDbService roleDynamoDbService;
    private CanvasRolePermissionDbService canvasRolePermissionDbService;

    public CanvasClient(Client client) {
        serviceClient = client;
        env = ServiceEnv.getInstance();
        canvasBaseURL = env.getEnv(CANVAS_URL_PROPERTY,"http://ec2-18-117-95-158.us-east-2.compute.amazonaws.com:8900/api/v1/");
        if(!canvasBaseURL.endsWith("/")) canvasBaseURL += "/";
        rootAccountId = env.getEnv(CANVAS_ROOT_ACCOUNT_PROPERTY,CANVAS_ROOT_ACCOUNT_ID);
        roleDynamoDbService = new RoleDynamoDbService();
        canvasRolePermissionDbService = new CanvasRolePermissionDbService();
    }

    public String getAllCourses() {
        return callCanvasAPI(COURSE_BASE_PATH,HttpMethod.GET,String.class,null);
    }

    public String getCoursesForAccount(String accountId) {
        return callCanvasAPI(ACCOUNT_BASE_PATH + accountId +"/courses",HttpMethod.GET,String.class,null);
    }

    public String getCoursesForUser(String userId) {
        return callCanvasAPI(USER_BASE_PATH + userId + "/courses",HttpMethod.GET,String.class,null);
    }

    public CanvasAccount createCanvasAccount(String accountName) {
        String encodedAccount = accountName;
        try {
            encodedAccount = URLEncoder.encode(accountName, StandardCharsets.UTF_8.name());
        }catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String basePath = ACCOUNT_BASE_PATH + CANVAS_ROOT_ACCOUNT_ID + "/sub_accounts?account[name]="+ encodedAccount;
        System.out.println("Canvas basepath = " + basePath);
        return callCanvasAPI(basePath,HttpMethod.POST,CanvasAccount.class,null);
    }

    public CanvasAccount deleteCanvasAccount(String accountId) {
        String basePath = ACCOUNT_BASE_PATH + CANVAS_ROOT_ACCOUNT_ID + "/sub_accounts/" + accountId;
        return callCanvasAPI(basePath, HttpMethod.DELETE, CanvasAccount.class, null);
    }

    public String createUser(String accountId, String schoolId, String userName, String email) {
        String encodedUserName = userName;

        try {
            encodedUserName = URLEncoder.encode(userName, StandardCharsets.UTF_8.name());
        }catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        Map<String, String> queryParams = new HashMap<>();
        queryParams.put(CanvasRequestParameterConstant.QUERY_PARAM_USER_NAME, encodedUserName);
        queryParams.put(CanvasRequestParameterConstant.QUERY_PARAM_USER_UNIQUE_ID, email);
        queryParams.put(CanvasRequestParameterConstant.QUERY_PARAM_USER_SIS_ID, schoolId);
        queryParams.put(CanvasRequestParameterConstant.QUERY_PARAM_USER_PASSWORD,
                CanvasRequestParameterConstant.QUERY_PARAM_USER_PASSWORD_DEFAULT);
        CanvasUser response = callCanvasAPI(ACCOUNT_BASE_PATH + accountId + "/users",
                HttpMethod.POST, CanvasUser.class, null, queryParams);
        return response.getId();
    }

    public String listUsersInAccount(String accountId) {
        String users = callCanvasAPI(ACCOUNT_BASE_PATH + accountId + "/users", HttpMethod.GET, String.class, null);

        return users;
    }

    private <T> T callCanvasAPI(String basePath,
                                 String method,
                                 Class<T> expectedResponseType, String entity) {
        return callCanvasAPI(basePath,method,expectedResponseType,entity,null);
    }

    /**
     * Overload callCanvasAPI method - Takes map of query parameters as arguments
     */
    private <T> T callCanvasAPI(String basePath,
                                String method,
                                Class<T> expectedResponseType, String entity, Map<String, String> queryStrings) {
        WebTarget webTarget = serviceClient.target(canvasBaseURL + basePath);

        if(queryStrings != null) {
            for (String key : queryStrings.keySet()) {
                String value = queryStrings.get(key);
                webTarget = webTarget.queryParam(key, value);
            }
        }

        System.out.println("Invoking Canvas at url " + canvasBaseURL + basePath);
        Response response = webTarget
                .request(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + env.getEnv(CANVAS_API_TOKEN_PROPERTY))
                .method(method, (entity != null) ? Entity.json(entity) : null);
        if (response.getStatus() == Response.Status.OK.getStatusCode()) {
            return response.readEntity(expectedResponseType);
        } else {
            String responseStr = response.readEntity(String.class);
            System.out.println("Got error response " + responseStr);
            ClientUtils.throwClientException(response);
            return null;
        }
    }

    /**
     * Add roles to an account (school) in Canvas. This method is used when creating a
     * school: after the school has been added as an organization to the Org Module
     * and an account has been created in Canvas for the school.
     *
     * @param account: The created canvas account for the school
     *
     */
    public void addCanvasAccountRoles(CanvasAccount account){
        // get list of defined roles from dynamoDb
        List<EducationRole> educationRoles = roleDynamoDbService.getRoles();
        // try adding roles if given account is valid and roles were retrieved from dynamoDB
        if(account.getId() != null && educationRoles.size() > 0){
            // Iterate through roles found in database
            for(EducationRole educationRole : educationRoles){
                //get list of permissions for each role
                List<CanvasRolePermission> permissions = canvasRolePermissionDbService
                        .getAllPermissionsByRole(educationRole.getRoleId());
                // only add a role for which permissions are found
                if(permissions.size() > 0){
                    // Get map of role request parameters
                    Map<String, String> queryStrings = getCreateRoleRequestParameters(educationRole.getRoleId(), permissions);
                    //submit request to add roles to account in Canvas
                    String basePath = ACCOUNT_BASE_PATH + account.getId() + ROLE_BASE_PATH;
                    CanvasRole canvasRole = callCanvasAPI(basePath, HttpMethod.POST, CanvasRole.class,null,
                            queryStrings);
                    System.out.println("Added: " + canvasRole.getRole()+ " to account '" + account.getName() + "' ");
                }else {
                    //log roles without found permissions
                    System.out.println("Skipped " + educationRole.getRoleId() + " - no defined permissions");

                }
            }
        }
    }

    /**
     * Get the query parameters needed for canvas create role request.
     *
     * @return Map of role name, role type and permission parameters for create role request
     */
    private Map<String, String> getCreateRoleRequestParameters(String role, List<CanvasRolePermission> permissions) {
        //add role label and role type
        Map<String, String> queryStrings = new HashMap<>();
        queryStrings.put(CanvasRequestParameterConstant.QUERY_PARAM_ROLE_LABEL, role);
        queryStrings.put(CanvasRequestParameterConstant.QUERY_PARAM_BASE_ROLE_TYPE, "AccountMembership");

        //add permissions
        for (CanvasRolePermission permission : permissions) {
            queryStrings.put(permission.getPermissionParameter(), permission.getPermissionValue());
        }
        return queryStrings;
    }

    /**
     * Get list of roles from a Canvas account
     *
     * Note: Default account roles are also retrieved from Canvas
     *
     * @param accountId
     * @return role list
     */
    private List<CanvasRole> getAccountRoles(String accountId){
        //Request list of roles from Canvas
        String basePath = ACCOUNT_BASE_PATH + accountId + ROLE_BASE_PATH;
        String accountRolesJSON = callCanvasAPI(basePath, HttpMethod.GET, String.class, null);

        List<CanvasRole> roles = new ArrayList<>();

        try {
            //maps id and role (name) only
            final ObjectMapper objectMapper = new ObjectMapper();
            roles = objectMapper.readValue(accountRolesJSON, new TypeReference<List<CanvasRole>>(){});
        } catch (JsonProcessingException e) {
            System.out.println("Mapping of roles failed");
        }
        return roles;
    };

    /**
     * Add Canvas account level roles to a user
     *
     * @param accountId
     * @param userId
     * @param role
     * @return details of the role assigned to user
     */
    public String assignAccountRoleToUser(String accountId, String userId, String role) {
        //request parameters
        Map<String, String> queryParams = new HashMap<>();

        if(role.equals("ADMIN")){
            queryParams.put(CanvasRequestParameterConstant.QUERY_PARAM_ADMIN_ROLEID, "AccountAdmin");
        }else {
            // Get list of roles in account
            List<CanvasRole> accountRoles = getAccountRoles(accountId);
            // Find role to add in list
            CanvasRole roleFound = accountRoles.stream()
                    .filter(canvasRole -> role.equals(canvasRole.getRole()))
                    .findAny()
                    .orElse(null);
            if(roleFound != null) {
                queryParams.put(CanvasRequestParameterConstant.QUERY_PARAM_ADMIN_ROLEID, roleFound.getId());
            }else {
                return null;
            }

        }

        queryParams.put(CanvasRequestParameterConstant.QUERY_PARAM_ADMIN_USERID, userId);
        //Disabling send confirmation for role assignment
        queryParams.put(CanvasRequestParameterConstant.QUERY_PARAM_ADMIN_SENDCONFIRMATION, String.valueOf(false));

        String requestPath = ACCOUNT_BASE_PATH + accountId + ROLE_ASSIGNMENT_BASE_PATH;
        String resultsJson = callCanvasAPI(requestPath, HttpMethod.POST, String.class, null, queryParams);

        return resultsJson;
    }

}
