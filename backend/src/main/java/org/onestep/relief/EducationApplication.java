package org.onestep.relief;

import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.jersey.jackson.JsonProcessingExceptionMapper;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.swagger.v3.jaxrs2.integration.resources.OpenApiResource;
import io.swagger.v3.oas.integration.SwaggerConfiguration;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.servers.Server;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.glassfish.jersey.logging.LoggingFeature;
import org.onestep.relief.resources.SchoolCollectionResource;
import org.onestep.relief.resources.CourseResource;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import javax.ws.rs.client.Client;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class EducationApplication extends Application<EducationConfig> {

  public static void main(String[] args) throws Exception {
    InputStream propertyStream = null;
    try {
      propertyStream = new FileInputStream(new File(System.getProperty("user.home"),"onestep-server-env.properties"));
    }catch (IOException e) {
      System.out.println("No custom environment properties found...");
    }
    if(propertyStream != null) {
      Properties props = new Properties();
        props.load(propertyStream);
      Map<String,String> overrides = props.entrySet().stream().collect(
              Collectors.toMap(
                      e -> String.valueOf(e.getKey()),
                      e -> String.valueOf(e.getValue()),
                      (prev,next) -> next, HashMap::new
              )
      );
      ServiceEnv.getInstance().initializeEnv(overrides,true);
    }
    new EducationApplication().run(args);
  }

  public String getName() {
    return "education-service";
  }

  public void initialize(Bootstrap<EducationConfig> bootstrap) {
    bootstrap.addBundle(new AssetsBundle("/swagger-ui", "/api-docs", "index.html"));
  }

  @Override
  public void run(EducationConfig educationConfig, Environment environment) throws Exception {
    //Initialize client
    final Client client = new JerseyClientBuilder(environment).using(educationConfig.getClientConfiguration())
            .build(getName());
    client.register(new LoggingFeature(Logger.getLogger(LoggingFeature.DEFAULT_LOGGER_NAME), Level.INFO,LoggingFeature.Verbosity.PAYLOAD_TEXT,10000));
    //Enable CORS
    final FilterRegistration.Dynamic corsFilter = environment.servlets().addFilter("CORS", CrossOriginFilter.class);
    corsFilter.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM,"*");
    corsFilter.setInitParameter(CrossOriginFilter.ALLOWED_HEADERS_PARAM,"X-Requested-With,Content-Type,Accept,Origin,Authorization");
    corsFilter.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "OPTIONS,GET,PUT,POST,DELETE,HEAD");
    corsFilter.setInitParameter(CrossOriginFilter.ALLOW_CREDENTIALS_PARAM, "true");
    corsFilter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
    corsFilter.setInitParameter("allowedHeaders",
            "Cache-Control,If-Modified-Since,Pragma,Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin");
    environment.jersey().register(new SchoolCollectionResource(client));
    //environment.jersey().register(new UserResource());
    environment.jersey().register(new CourseResource(client));
    environment.jersey().register(new JsonProcessingExceptionMapper(true));
    environment.healthChecks().register("education-health",new EducationHealthCheck());

    initSwagger(environment);
  }

  private void initSwagger(Environment environment) {
    OpenAPI oas = new OpenAPI();
    Info info
      = new Info().title("Education Module")
                  .description("Blah Blah Blah");

    oas.info(info);
    oas.addServersItem(new Server().url("/education"));
    SwaggerConfiguration oasConfig
      = new SwaggerConfiguration().openAPI(oas)
                                  .prettyPrint(true)
                                  .resourcePackages(Stream.of("org.onestep.relief.resources")
                                  .collect(Collectors.toSet()));
    environment.jersey().register(new OpenApiResource().openApiConfiguration(oasConfig));
  }
}
