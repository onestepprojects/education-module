package org.onestep.relief.representations;


import com.fasterxml.jackson.annotation.JsonAlias;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Map;

public class PersonRepresentation {


    String[] roles;

    String school;

    @JsonAlias("id")
    @Size(min = 4)
    private String uuid;

    @NotNull
    private String name;

    //@Valid
    //private RepAddress currentAddress;

    @Email
    private String email;

    @Size(min = 1)
    private Map<String, String> phones;

    /*
     * Fields for Person
     */

    private String birthdate;
    //private FileRepr profilePicture;
    private String lastActivity;
    //private List<EventRepr> history;
   // private RepAddress homeAddress;
    private String ecUUID;
    private String ecName;
    private Map<String, String> ecPhones;
    //private RepAddress ecAddress;
    //private PaymentAccountRepr paymentAccount;
    //private Map<String, RepLocation> location;
    private String gender;
    private Map<String, String> socialMedia;

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String[] getRoles() {
        return roles;
    }

    public void setRoles(String[] roles) {
        this.roles = roles;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Map<String, String> getPhones() {
        return phones;
    }

    public void setPhones(Map<String, String> phones) {
        this.phones = phones;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getLastActivity() {
        return lastActivity;
    }

    public void setLastActivity(String lastActivity) {
        this.lastActivity = lastActivity;
    }

    public String getEcUUID() {
        return ecUUID;
    }

    public void setEcUUID(String ecUUID) {
        this.ecUUID = ecUUID;
    }

    public String getEcName() {
        return ecName;
    }

    public void setEcName(String ecName) {
        this.ecName = ecName;
    }

    public Map<String, String> getEcPhones() {
        return ecPhones;
    }

    public void setEcPhones(Map<String, String> ecPhones) {
        this.ecPhones = ecPhones;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Map<String, String> getSocialMedia() {
        return socialMedia;
    }

    public void setSocialMedia(Map<String, String> socialMedia) {
        this.socialMedia = socialMedia;
    }

    public boolean hasRole(String role) {
        boolean has = false;
        if(roles != null) {
            for(String r: roles) {
                if(r.equals(role)) has = true;
            }
        }
        return has;
    }

    public boolean hasAnyRole(String roles[]) {
        boolean has = false;
        for(String r: roles) {
            has = hasRole(r);
            if(has) break;
        }
        return has;
    }

    public boolean equals(Object other) {
        if(other == null || !(other instanceof PersonRepresentation)) return false;
        return ((PersonRepresentation)other).getName().equals(getName());
    }

    public int hashCode() {
        return getName().hashCode();
    }


}
