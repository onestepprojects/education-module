package org.onestep.relief.representations;

import java.util.List;

public class RolesRepresentation {
    private String id;
    private String name;
    private String[] roles;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getRoles() {
        return roles;
    }

    public void setRoles(String[] roles) {
        this.roles = roles;
    }

    public String toString() {
        String output = "";

        //output += "roles: ";
        output += getId() + " " + getName() + " ";
        for (int i = 0; i < getRoles().length; i++) {
            output += roles[i];
        }

        return output;
    }
}
