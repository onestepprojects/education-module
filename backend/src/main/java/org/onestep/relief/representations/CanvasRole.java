package org.onestep.relief.representations;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Represent canvas role object
 *
 *Note: Role object from Canvas contains many additional properties.
 * Id, Role only are retrieved
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CanvasRole {
    
    private String id;
    private String role;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
