package org.onestep.relief.representations;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Map;

/**
 * Response entity for a school
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SchoolRepresentation {

    private String id;
    private String name;
    private Address address;
    @Pattern(regexp = "FOR_PROFIT|NON_PROFIT|FOUNDATION|GOVERNMENTAL|EDUCATIONAL|HYBRID")
    private String type;
    private String tagline;
    @Email
    private String email;

    @Pattern(regexp = "INITIATING|ACTIVE|INACTIVE|PAUSED|CLOSED")
    private String statusLabel;

    @Pattern(regexp = "[0-9]{13}")
    private String statusLastActive;

    public String getStatusLabel() {
        return statusLabel;
    }

    public void setStatusLabel(String statusLabel) {
        this.statusLabel = statusLabel;
    }

    public String getStatusLastActive() {
        return statusLastActive;
    }

    public void setStatusLastActive(String statusLastActive) {
        this.statusLastActive = statusLastActive;
    }

    public String getTagline() {
        return tagline;
    }

    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Map<String, String> getPhones() {
        return phones;
    }

    public void setPhones(Map<String, String> phones) {
        this.phones = phones;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    @Size(min = 1)
    private Map<String, String> phones;

    @Size(min = 2)
    private String website;

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty
    public Address getStreetAddress() {
        return address;
    }

    public void setStreetAddress(Address address) {
        this.address = address;
    }

}
