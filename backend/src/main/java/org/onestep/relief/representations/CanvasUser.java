package org.onestep.relief.representations;

/**
 *  This class represents user from canvas
 *  https://canvas.instructure.com/doc/api/users.html#User
 */
public class CanvasUser {

    String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


}
