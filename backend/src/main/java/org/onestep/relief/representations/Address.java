package org.onestep.relief.representations;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class Address {
    @Pattern(regexp = ".+ .+")
    private String address;

    @Size(min = 3)
    private String city;

    @Size(min = 1)
    private String district;

    @Size(min = 2)
    private String state;

    @Size(min = 2)
    private String postalCode;

    @Size(min = 2)
    private String country;
    
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }


}
