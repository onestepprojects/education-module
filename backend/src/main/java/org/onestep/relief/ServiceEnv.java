package org.onestep.relief;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class ServiceEnv {

  private static final String USER_CONFIG_FILE = "onestep-server-env.properties";

  private volatile boolean initialized;

  private Map<String, String> envMap = new ConcurrentHashMap<>();

  private ServiceEnv() {
    initializeEnv(null,false);
  }

  public synchronized void initializeEnv(Map<String,String> overrides, boolean force) {
    if(!initialized || force) {
      Map<String, String> newMap = new HashMap<>();
      //Add all system environment variables
      newMap.putAll(System.getenv());
      //User properties - if present - can override system properties
      Map<String,String> userProperties = getUserProperties();
      if(userProperties != null) newMap.putAll(userProperties);
      //Add Java system properties. System properties are generally specific to the JVM
      // (Ex: -Dmyarg=test.
      //System properties override environment variables and user properties
      newMap.putAll(System.getProperties().entrySet().stream().collect(
              Collectors.toMap(x -> x.getKey().toString(), x -> x.getValue().toString())
      ));
      //finally add the overrides which can be used to override anything
      //overrides are useful within test code
      if(overrides != null) {
        newMap.putAll(overrides);
      }

      envMap.clear();
      envMap.putAll(newMap);
      initialized = true;
    }

  }

  private Map<String,String> getUserProperties() {
    Map<String,String> props = null;
    File f = new File(System.getenv("user.home"),USER_CONFIG_FILE);
    if(f.exists()) {
      try(FileInputStream fis = new FileInputStream(f)) {
        Properties p = new Properties();
        p.load(fis);
        p.stringPropertyNames().stream().forEach((name) -> {
          props.put(name, p.getProperty(name));
        });
      }catch (IOException e) {
        //Just log the error
        System.out.println("Error occurred while reading user property file: " + e.getMessage());
      }

    }
    return props;
  }

  /**
   * Gets the value for the environment variable specified by envPropertyName or null if it does not exist.
   * @param envPropertyName The name of the environment variable
   * @return The value of the environment variable or null if the variable has not been set
   */
  public String getEnv(String envPropertyName) {
    return getEnv(envPropertyName,null);
  }

  /**
   * Gets the value of an environment variable or the specified default if that variable does not exist
   * @param envPropertyName The name of the environment variable
   * @param defaultValue The default value to return if the environment variable was not set.
   * @return The value of the environment variable or defaultValue if the variable was not set.
   */
  public String getEnv(String envPropertyName, String defaultValue) {
    return envMap.getOrDefault(envPropertyName,defaultValue);
  }

  public void setEnv(String envPropertyName, String value) {
    envMap.put(envPropertyName,value);
  }

  public static ServiceEnv getInstance() {
    return EnvironmentHolder.ENVIRONMENT_INSTANCE;
  }



  private static class EnvironmentHolder {

    public static final ServiceEnv ENVIRONMENT_INSTANCE = new ServiceEnv();
  }

}
