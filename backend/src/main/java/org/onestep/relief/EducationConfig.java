package org.onestep.relief;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import io.dropwizard.client.JerseyClientConfiguration;
import io.dropwizard.util.Duration;

public class EducationConfig extends Configuration {


    private JerseyClientConfiguration clientConfiguration;

    public EducationConfig() {
        clientConfiguration = new JerseyClientConfiguration();
        clientConfiguration.setConnectionRequestTimeout(Duration.seconds(30));
        clientConfiguration.setConnectionTimeout(Duration.seconds(30));
        clientConfiguration.setTimeout(Duration.seconds(30));


    }


    @JsonProperty("jerseyClient")
    public JerseyClientConfiguration getClientConfiguration() {
        return clientConfiguration;
    }

    @JsonProperty("jerseyClient")
    public void setClientConfiguration(JerseyClientConfiguration clientConfiguration) {
        this.clientConfiguration = clientConfiguration;
    }

}
