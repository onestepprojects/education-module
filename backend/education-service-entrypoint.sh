#!/bin/sh
set -e #exit immediately if a command exits with a non-zero status.
if [[ -z ${SERVER_PORT} ]]; then
  SERVER_PORT="8080"
fi
if [[ -z ${ADMIN_PORT} ]]; then
  ADMIN_PORT="8081"
fi

java -D"dw.server.applicationConnectors[0].port=$SERVER_PORT" -D"dw.server.adminConnectors[0].port=$ADMIN_PORT" -jar /education-service/target/education-1.0-SNAPSHOT.jar server
#