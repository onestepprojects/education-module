import { createApi } from './base'
import { SCHOOLS } from './mockup'

/*
Example uses:

import api from './api/schools'

// Get all schools
const schools = await api.getAll()

// Get schools some filters, e.g. by admin id
// Note: the details shall be implemented by the back-end
const schools = await api.getAll({ adminId: 'userId'})

// Get a school by id
const school = await api.getById('schoolId')

// Add a school
await api.create({ id: ..., name: ... })

// Update a school
await api.update({ id: ..., name: ... })

// Remove a school
await api.remove('schoolId')
*/
export default createApi({
  endpoint: 'schools',
})
