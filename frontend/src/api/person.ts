import { apiFetch } from '../api/base'

const BASE_URL = process.env.REACT_APP_ORG_MODULE_API_URL || 'http://localhost:9090'

export default {
  getAll: async () => apiFetch(BASE_URL, 'persons', 'GET'),

  updateRole: async ({ id, schoolId, role }) =>
    apiFetch(BASE_URL, `composite/update-person-role-status/${role}/${schoolId}/${id}/ACTIVE`),
}
