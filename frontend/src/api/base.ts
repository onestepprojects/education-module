const BASE_URL = process.env.REACT_APP_EDUCATION_MODULE_API_URL || 'http://localhost:8080'

export async function apiFetch(
  baseUrl: string,
  endpoint: string,
  method: string = 'GET',
  body?: any,
  mockupData?: any
) {
  const options = {
    method,
    headers: {
      'Content-Type': 'application/json',
    },
  }

  const token = localStorage.getItem('authtoken')
  if (token) {
    options.headers['Authorization'] = `Bearer ${token}`
  }

  if (method !== 'GET' && body != null) {
    options['body'] = JSON.stringify(body)
  }

  const url =
    method === 'GET' && body
      ? `${baseUrl || BASE_URL}/${endpoint}?` + new URLSearchParams(body)
      : `${baseUrl || BASE_URL}/${endpoint}`
  console.log(`${method} ${url} ...`)

  try {
    const response = await fetch(url, options)
    if (!response.ok) {
      throw new Error(`Response ${response.status} - ${response.statusText}`)
    }
    return response.text().then((text) => {
      // Handle error where the back-end does not respect the API design
      try {
        return JSON.parse(text)
      } catch (e) {
        console.warn(`${method} ${endpoint}: response is not a valid JSON:`, text)
        return text
      }
    })
  } catch (error) {
    if (mockupData) {
      console.warn(`${method} ${endpoint}: ${error} - Use mockup data`, mockupData)
      return mockupData
    } else {
      throw new Error(`${method} ${endpoint}: ${error}`)
    }
  }
}

type CRUDAPI = {
  getById: (string) => Promise<any>
  getAll: (object?) => Promise<any>
  create: (object) => Promise<any>
  update: (object) => Promise<any>
  remove: (string) => Promise<any>
}

export function createApi({ endpoint, mockupData = null, baseUrl = null }): CRUDAPI {
  return {
    getById: async (id) =>
      apiFetch(
        baseUrl || BASE_URL,
        `${endpoint}/${id}`,
        'GET',
        null,
        mockupData && mockupData.filter((obj) => obj.id === id)
      ),

    getAll: async (filter) => apiFetch(baseUrl || BASE_URL, endpoint, 'GET', filter, mockupData),

    create: async (data) => apiFetch(baseUrl || BASE_URL, endpoint, 'POST', data, null),

    update: async (data) =>
      apiFetch(baseUrl || BASE_URL, `${endpoint}/${data.id}`, 'PUT', data, null),
    remove: (id) => apiFetch(baseUrl || BASE_URL, `${endpoint}/${id}`, 'DELETE', null, null),
  }
}
