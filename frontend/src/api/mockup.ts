/*
 * Mockup data for testing the back-end API calls
 */

export const SCHOOLS = [
  {
    id: 'f15d22a6-ea8b-44a1-b335-f9b3b9c7ebbe',
    name: 'Sample University',
    address: {
      address: '200 Harvard Square',
      city: 'Boston',
      district: null,
      state: 'MA',
      postalCode: null,
      country: null,
    },
    type: 'EDUCATIONAL',
    tagline: null,
    email: null,
    statusLabel: 'OrganizationStatus(status=ACTIVE, lastActive=Tue Apr 05 14:43:12 UTC 2022)',
    statusLastActive: '1649169792843',
    phones: {
      phone: '1111',
      mobile: '1111',
      sat_phone: '1111',
    },
    website: null,
    streetAddress: {
      address: '200 Harvard Square',
      city: 'Boston',
      district: null,
      state: 'MA',
      postalCode: null,
      country: null,
    },
  },
]

export const USERS = [
  {
    id: 'test-user-001',
    name: 'HES_User1',
    email: 'hu1@g.harvard.edu',
    role: 'School Administrator',
    schoolId: 'test-school-001',
  },
  {
    id: 'test-user-002',
    name: 'HES_User2',
    email: 'hu2@g.harvard.edu',
    role: 'Educator',
    schoolId: 'test-school-001',
  },
  {
    id: 'test-user-003',
    name: 'HES_User3',
    email: 'hu3@g.harvard.edu',
    role: 'Student',
    schoolId: 'test-school-001',
  },
  {
    id: 'test-user-004',
    name: 'HES_User4',
    email: 'hu4@g.harvard.edu',
    role: 'Student',
    schoolId: 'test-school-001',
  },
  {
    id: 'test-user-005',
    name: 'HES_User5',
    email: 'hu5@g.harvard.edu',
    role: 'Student',
    schoolId: 'test-school-001',
  },
]

export const COURSES = [
  {
    id: 'test-course-001',
    title: 'Intro to Biology',
    category: 'BIOL',
    educatorId: 'test-user-0002',
    progress: 50,
  },
  {
    id: 'test-course-002',
    title: 'Intro to Chemistry',
    category: 'CHEM',
    educatorId: 'test-user-0002',
    progress: 50,
  },
  {
    id: 'test-course-003',
    title: 'Intro to Physics',
    category: 'PHYS',
    educatorId: 'test-user-0002',
    progress: 50,
  },
  {
    id: 'test-course-004',
    title: 'Intro to Mathematics',
    category: 'MATH',
    educatorId: 'test-user-0002',
    progress: 50,
  },
  {
    id: 'test-course-005',
    title: 'Intro to Computer Science',
    category: 'CSCI',
    educatorId: 'test-user-0002',
    progress: 50,
  },
  {
    id: 'test-course-006',
    title: 'Intro to Biology',
    category: 'BIOL',
    educatorId: 'test-user-0002',
    progress: 100,
  },
  {
    id: 'test-course-007',
    title: 'Intro to Chemistry',
    category: 'CHEM',
    educatorId: 'test-user-0002',
    progress: 100,
  },
  {
    id: 'test-course-008',
    title: 'Intro to Physics',
    category: 'PHYS',
    educatorId: 'test-user-0002',
    progress: 100,
  },
]
