import { apiFetch, createApi } from './base'
import { USERS } from './mockup'

/*
Example uses:

import api from './api/users'

// Get all users
const users = await api.getAll()

// Get users some filters, e.g. by role
// Note: the details shall be implemented by the back-end
const educators = await api.getAll({ role: 'Educator'})

// Get a user by id
const user = await api.getById('userId')

// Add a user
await api.create({ id: ..., name: ... })

// Update a user
await api.update({ id: ..., name: ... })

// Remove a user
await api.remove('userId')
*/
const api = createApi({
  endpoint: 'users',
})

// Create user ~ register a person (ORG) as as a EM's user
api.create = async (user) =>
  apiFetch(
    null,
    `schools/registerUser/${user.schoolId}/${user.id}/${user.role}/${user.name}/${user.email}`,
    'POST'
  )

// List of registered users
api.getRegisteredUsers = async (registered) =>
  apiFetch(null, `schools/users?registered=${registered}`)

// List of user by id
api.getUserByID = async (uuid) => apiFetch(null, `schools/users/${uuid}`)

export default api
