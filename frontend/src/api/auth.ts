import Amplify, { Auth, Hub } from 'aws-amplify'
import { apiFetch } from './base'

Amplify.configure({
  userPoolId: process.env.REACT_APP_USER_POOL_ID,
  region: process.env.REACT_APP_USER_POOL_REGION,
  identityPoolRegion: process.env.REACT_APP_USER_POOL_IDENTITY_POOL_REGION,
  userPoolWebClientId: process.env.REACT_APP_USER_POOL_USER_POOL_WEB_CLIENT_ID,
  oauth: {
    domain: process.env.REACT_APP_USER_POOL_DOMAIN,
    scope: ['openid', 'profile'],
    redirectSignIn: process.env.REACT_APP_USER_POOL_REDIRECT_SIGN_IN,
    redirectSignOut: process.env.REACT_APP_USER_POOL_REDIRECT_SIGN_OUT,
    responseType: 'code',
  },
})

export async function authentication() {
  try {
    if (process.env.REACT_APP_EDUCATION_MODULE_TEST_AUTH_TOKEN) {
      // Dev test
      localStorage.setItem('authtoken', process.env.REACT_APP_EDUCATION_MODULE_TEST_AUTH_TOKEN)
    } else {
      const user = await Auth.currentAuthenticatedUser()
      const token = await user.getSignInUserSession().getAccessToken().getJwtToken()
      localStorage.setItem('authtoken', token)
      return user
    }
  } catch (e) {
    console.warn(`[authentication] error ${e}`)
    Auth.federatedSignIn()
    return null
  }
}

export async function signOut() {
  try {
    localStorage.removeItem('authtoken')
    await Auth.signOut()
  } catch (error) {
    console.log('[authentication] error signing out: ', error)
  }
}
