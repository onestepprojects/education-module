import { apiFetch, createApi } from './base'
import { COURSES } from './mockup'

/*
Example uses:

import api from './api/courses'

// Get all courses
const courses = await api.getAll()

// Get courses some filters, e.g. by school
// Note: the details shall be implemented by the back-end
const courses = await api.getAll({ schoolId: 'some-school-id'})

// Get a course by id
const course = await api.getById('courseId')

// Add a course
await api.create({ id: ..., name: ... })

// Update a course
await api.update({ id: ..., name: ... })

// Remove a course
await api.remove('courseId')
*/
const api = createApi({
  endpoint: 'courses',
})

// List of all courses in EM
api.getExistingCourses = async () => apiFetch(null, `courses`)

// Get Canvas Account ID
api.getCanvasAccountID = async (uuid) => apiFetch(null, `schools/canvasAccountId/${uuid}`)

// Get Canvas User ID
api.getCanvasUserID = async (uuid) => apiFetch(null, `schools/canvasUserId/${uuid}`)

// List of available courses in a school
api.getSchoolCoursesByID = async (uuid) => {
  let cvid = await api.getCanvasAccountID(uuid)
  return apiFetch(null, `courses/account/${cvid}`)
}

// List of courses by ID
api.getUserCoursesByID = async (uuid) => {
  let cvid = await api.getCanvasUserID(uuid)
  return apiFetch(null, `courses/user/${cvid}`)
}

export default api
