// eslint-disable-next-line no-use-before-define
import React from 'react'
import { render, screen } from '@testing-library/react'
import App from './App'

describe('App', () => {
  it('shows "One Step" on page', () => {
    render(<App match={undefined} mountPath={undefined} auth={{
      token: '',
      person: undefined,
      roles: []
    }} history={undefined} location={undefined} />)
    const oneStepText = screen.getByText(/One Step/i)
    expect(oneStepText).toBeInTheDocument()
  })
})
