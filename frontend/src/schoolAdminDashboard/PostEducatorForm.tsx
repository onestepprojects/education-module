import React, { FC, useState, useEffect } from 'react'
import { Form, Input, Checkbox, Select, Cascader, FormInstance } from 'antd'
import { Typography, Space } from 'antd'
const { Text } = Typography
import {} from '@ant-design/icons'
import { FieldData } from 'rc-field-form/es/interface'

interface PostEducatorFormProps {
  form: FormInstance
  persons?: any[]
  schools?: any[]
  user?: any
  forDelete?: boolean
}

const PostEducatorForm: FC<PostEducatorFormProps> = ({
  form,
  user,
  persons,
  schools,
  forDelete,
}) => {
  const [fields, setFields] = useState<FieldData[]>([])

  useEffect(() => {
    setFields([
      {
        name: 'userID',
        value: user && user.id,
      },
      {
        name: 'username',
        value: user && user.name,
      },
      {
        name: 'email',
        value: user && user.email,
      },
      {
        name: 'role',
        value: user ? user.role : 'TEACHER',
      },
      {
        name: 'school',
        value: user ? user.schoolId : schools[0].id,
      },
      {
        name: 'deletionConfirm',
        value: false,
      },
    ])
  }, [user])

  if (forDelete) {
    return (
      <Form
        form={form}
        fields={fields}
        onFieldsChange={(_, newFields) => setFields(newFields)}
        name='basic'
        labelCol={{
          span: 5,
        }}
        wrapperCol={{
          span: 16,
        }}
        initialValues={{
          confirm: false,
        }}
        autoComplete='off'
      >
        <Form.Item
          label='USER ID'
          name='userID'
          rules={[
            {
              required: true,
              message: 'Select user to unregister',
            },
          ]}
        >
          <Input disabled={true} />
        </Form.Item>

        <Form.Item
          wrapperCol={{
            offset: 5,
            span: 16,
          }}
        >
          <Typography.Title type='warning' level={5} style={{ margin: 0 }}>
            WARNING
          </Typography.Title>

          <Text type='warning'>
            <p>
              Removal of a Educator will prevent further access to it from its resources and
              detaching their profiles from it indefinitely.
            </p>
          </Text>
        </Form.Item>

        <Form.Item
          name='deletionConfirm'
          valuePropName='checked'
          wrapperCol={{
            offset: 5,
            span: 16,
          }}
          rules={[
            {
              required: true,
              transform: (value) => value || undefined,
              type: 'boolean',
              message: 'Please tick the checkbox to confirm your action!',
            },
          ]}
        >
          <Checkbox>I Understand</Checkbox>
        </Form.Item>
      </Form>
    )
  } else {
    return (
      <Form
        form={form}
        fields={fields}
        onFieldsChange={(_, newFields) => setFields(newFields)}
        name='basic'
        labelCol={{
          span: 5,
        }}
        wrapperCol={{
          span: 16,
        }}
        initialValues={{
          remember: true,
        }}
        autoComplete='off'
      >
        <Form.Item
          label='USER ID'
          name='userID'
          rules={[
            {
              required: true,
              message: 'Please select a person!',
            },
          ]}
        >
          <Select
            style={{ width: '100%' }}
            showSearch
            placeholder='Select a person'
            optionFilterProp='children'
            onSelect={(uuid) => {
              const selectedPerson = persons.find((p) => p.uuid === uuid)
              console.log(`onSelect`, selectedPerson)
              setFields([
                {
                  name: 'email',
                  value: selectedPerson.email,
                },
                {
                  name: 'username',
                  value: selectedPerson.name,
                },
              ])
            }}
            filterOption={(input, option) => {
              const optionText = Array.isArray(option.children)
                ? option.children.join('')
                : option.children
              return optionText.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }}
          >
            {persons.map((p) => (
              <Select.Option key={p.uuid} value={p.uuid}>
                {p.name} ({p.uuid})
              </Select.Option>
            ))}
          </Select>
        </Form.Item>

        <Form.Item
          label='USERNAME'
          name='username'
          rules={[
            {
              required: false,
              message: 'Please input name of the user!',
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label='EMAIL'
          name='email'
          rules={[
            {
              required: true,
              message: 'Please input email of the user!',
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label='ROLE'
          name='role'
          initialValue='TEACHER'
          rules={[
            {
              required: true,
              message: 'Please input role of the user!',
            },
          ]}
        >
          <Select style={{ width: '100%' }} placeholder='Select a role'>
            <Select.Option value='ADMIN'>School Administrator</Select.Option>
            <Select.Option value='TEACHER'>Educator</Select.Option>
            <Select.Option value='STUDENT'>Student</Select.Option>
          </Select>
        </Form.Item>

        <Form.Item
          label='SCHOOL'
          name='school'
          rules={[
            {
              required: true,
              message: 'Please choose a school!',
            },
          ]}
        >
          <Select
            style={{ width: '100%' }}
            showSearch
            placeholder='Select a school'
            optionFilterProp='children'
            filterOption={(input, option) => {
              const optionText = Array.isArray(option.children)
                ? option.children.join('')
                : option.children
              return optionText.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }}
          >
            {schools.map((school) => (
              <Select.Option key={school.id} value={school.id}>
                {school.name}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
      </Form>
    )
  }
}

export default PostEducatorForm
