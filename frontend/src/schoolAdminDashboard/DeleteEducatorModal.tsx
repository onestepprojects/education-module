import React, { useState } from 'react'
import { Button, Modal, Form } from 'antd'
import PostEducatorForm from './PostEducatorForm'
import usersApi from '../api/users'

interface DeleteEducatorModalProps {
  visible: boolean
  user: any
  onCancel: () => void
  onFinish: (any?) => void
}

const DeleteEducatorModal: React.FC<DeleteEducatorModalProps> = ({
  visible,
  user,
  onCancel,
  onFinish,
}) => {
  const [form] = Form.useForm()
  const [loading, setLoading] = React.useState(false)

  const handleOk = async () => {
    setLoading(true)
    try {
      const values = await form.validateFields()
      console.log('[DeleteEducatorModal] form validate success:', values)

      const result = await usersApi.remove(user.id)
      console.log('[DeleteEducatorModal] updated user:', result)
      form.resetFields()
      onFinish()
    } catch (e) {
      console.log('[DeleteEducatorModal] submit error:', e)
      if (e instanceof Error) alert(e)
    } finally {
      setLoading(false)
    }
  }

  return (
    <Modal
      title='DELETE EXISTING TEACHER'
      width={750}
      visible={visible}
      onCancel={onCancel}
      footer={[
        <Button key='back' onClick={onCancel}>
          Cancel
        </Button>,
        <Button key='submit' type='primary' loading={loading} onClick={handleOk}>
          Submit
        </Button>,
      ]}
    >
      <PostEducatorForm form={form} user={user} forDelete={true} />
    </Modal>
  )
}

export default DeleteEducatorModal
