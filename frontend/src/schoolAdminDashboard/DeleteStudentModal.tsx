import React, { useState } from 'react'
import { Button, Modal, Form } from 'antd'
import PostStudentForm from './PostStudentForm'
import usersApi from '../api/users'

interface DeleteStudentModalProps {
  visible: boolean
  user: any
  onCancel: () => void
  onFinish: (any?) => void
}

const DeleteStudentModal: React.FC<DeleteStudentModalProps> = ({
  visible,
  user,
  onCancel,
  onFinish,
}) => {
  const [form] = Form.useForm()
  const [loading, setLoading] = React.useState(false)

  const handleOk = async () => {
    setLoading(true)
    try {
      const values = await form.validateFields()
      console.log('[DeleteStudentModal] form validate success:', values)

      const result = await usersApi.remove(user.id)
      console.log('[DeleteStudentModal] updated user:', result)
      form.resetFields()
      onFinish()
    } catch (e) {
      console.log('[DeleteStudentModal] submit error:', e)
      if (e instanceof Error) alert(e)
    } finally {
      setLoading(false)
    }
  }

  return (
    <Modal
      title='DELETE EXISTING STUDENT'
      width={750}
      visible={visible}
      onCancel={onCancel}
      footer={[
        <Button key='back' onClick={onCancel}>
          Cancel
        </Button>,
        <Button key='submit' type='primary' loading={loading} onClick={handleOk}>
          Submit
        </Button>,
      ]}
    >
      <PostStudentForm form={form} user={user} forDelete={true} />
    </Modal>
  )
}

export default DeleteStudentModal
