import React, { useState } from 'react'
import { Button, Modal, Form } from 'antd'

import usersApi from '../api/users'
import PostStudentForm from './PostStudentForm'

interface UpdateStudentModalProps {
  visible: boolean
  schools: any[]
  user: any
  onCancel: () => void
  onFinish: (any?) => void
}

const UpdateStudentModal: React.FC<UpdateStudentModalProps> = ({
  visible,
  user,
  schools,
  onCancel,
  onFinish,
}) => {
  const [form] = Form.useForm()
  const [loading, setLoading] = React.useState(false)

  const handleOk = async () => {
    setLoading(true)
    try {
      const values = await form.validateFields()
      console.log('[UpdateStudentModal] form validate success:', values)

      const result = await usersApi.update({
        id: user.id,
        name: values.username,
        email: values.email,
        role: values.role,
        schoolId: values.school,
      })
      console.log('[UpdateStudentModal] updated user:', result)
      onFinish()
    } catch (e) {
      console.log('[UpdateSchoolModal] submit error:', e)
      if (e instanceof Error) alert(e)
    } finally {
      setLoading(false)
    }
  }

  return (
    <Modal
      title='UPDATE EXISTING USER'
      width={750}
      visible={visible}
      onCancel={onCancel}
      footer={[
        <Button key='back' onClick={onCancel}>
          Cancel
        </Button>,
        <Button key='submit' type='primary' loading={loading} onClick={handleOk}>
          Submit
        </Button>,
      ]}
    >
      <PostStudentForm form={form} user={user} schools={schools} />
    </Modal>
  )
}

export default UpdateStudentModal
