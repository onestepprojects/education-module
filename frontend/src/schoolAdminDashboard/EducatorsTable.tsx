import React, { FC, useEffect, useState } from 'react'
import { Table, Avatar, Divider, Button, Tooltip } from 'antd'
import { InfoCircleOutlined, UserOutlined, DeleteOutlined, PlusOutlined } from '@ant-design/icons'

import PostEducatorModal from './PostEducatorModal'
import UpdateEducatorModal from './UpdateEducatorModal'
import DeleteEducatorModal from './DeleteEducatorModal'

import usersApi from '../api/users'

const userColumns = [
  {
    title: () => (
      <a>
        <InfoCircleOutlined className='padded' />
      </a>
    ),
    dataIndex: '',
    key: 'x',
    render: () => (
      <a>
        <Avatar size={20} icon={<UserOutlined />} />
      </a>
    ),
  },
  {
    title: 'ID',
    dataIndex: 'id',
    filters: [],
    // specify the condition of filtering result
    // here is that finding the name started with `value`
    onFilter: (value, record) => record.id.indexOf(value) === 0,
    sorter: (a, b) => a.id.length - b.id.length,
    sortDirections: ['descend'],
  },
  {
    title: 'NAME',
    dataIndex: 'name',
    defaultSortOrder: 'descend',
    sorter: (a, b) => a.name - b.name,
  },
  {
    title: 'EMAIL',
    dataIndex: 'email',
    defaultSortOrder: 'descend',
    sorter: (a, b) => a.email - b.email,
  },
  {
    title: 'ROLE',
    dataIndex: 'role',
    defaultSortOrder: 'descend',
    sorter: (a, b) => a.role - b.role,
  },
  /*{
    title: 'SCHOOL',
    dataIndex: 'schoolId',
    defaultSortOrder: 'descend',
    sorter: (a, b) => a.schoolId - b.schoolId,
  },*/
]

function s(table, onEdit, onDelete) {
  let hasSettings = false
  for (let i = 0; i < table.length; i++) {
    if (table[i].title == 'SETTINGS') {
      hasSettings = true
    }
  }
  if (!hasSettings) {
    table.push({
      title: 'SETTINGS',
      dataIndex: '',
      key: 'x',
      render: (text, record, index) => (
        <a>
          {/* <Button shape='circle' icon={<EditOutlined />} onClick={() => onEdit(record)} /> */}
          <Button shape='circle' icon={<DeleteOutlined />} onClick={() => onDelete(record)} />
        </a>
      ),
    })
  }
  return table
}

interface EducatorsTableProps {
  user: object
}

const EducatorsTable: FC<EducatorsTableProps> = ({ user }) => {
  const [loading, setLoading] = useState(false)
  const [data, setData] = useState([])
  const [lastUpdate, setLastUpdate] = useState(Date.now())

  const [showAddModal, setShowAddModal] = useState(false)
  const [showEditModal, setShowEditModal] = useState(false)
  const [showDeleteModal, setShowDeleteModal] = useState(false)
  const [editingItem, setEditingItem] = useState()
  const [deletingItem, setDeletingItem] = useState()

  useEffect(() => {
    setLoading(true)
    usersApi
      .getRegisteredUsers(true)
      .then((persons) =>
        persons.map((p) => ({
          id: p.uuid,
          name: p.name,
          email: p.email,
          role: p.roles.includes('GLOBAL_ADMIN')
            ? 'Global Administrator'
            : p.roles.includes('ADMIN')
            ? 'School Administrator'
            : p.roles.includes('TEACHER')
            ? 'Teacher'
            : 'Student',
          schoolId: p.school,
        }))
      )
      .then((info) => {
        for (let i = 0; i < info.length; i++) {
          if (user.schoolId != info[i].schoolId || info[i].role != 'Teacher') {
            info.splice(i, 1)
            i--
          }
        }
        return info
      })
      .then((people) => people.sort((a, b) => a.name.localeCompare(b.name)))
      .then(setData)
      .catch((e) => console.error(`[EducatorsTable] error loading users API ${e}`))
      .finally(() => setLoading(false))
  }, [lastUpdate])

  const onEdit = (item) => {
    setEditingItem(item)
    setShowEditModal(true)
  }
  const onDelete = (item) => {
    setDeletingItem(item)
    setShowDeleteModal(true)
  }

  const onFinishModal = () => {
    setShowAddModal(false)
    setShowEditModal(false)
    setShowDeleteModal(false)
    setLastUpdate(Date.now())
  }

  //console.log(data)
  const schools = [user.school] // the only school

  return (
    <>
      <Divider className='user-role-container' orientation='left'>
        <span style={{ paddingRight: '1em' }}>Educators</span>
        <Button shape='circle' icon={<PlusOutlined />} onClick={() => setShowAddModal(true)} />
      </Divider>
      <div className='user-container'>
        <Table
          columns={s(userColumns, onEdit, onDelete)}
          loading={loading}
          dataSource={data}
          pagination={{ pageSize: 10 }}
        />
      </div>
      <PostEducatorModal
        visible={showAddModal}
        schools={schools}
        onCancel={() => setShowAddModal(false)}
        onFinish={onFinishModal}
      />
      <UpdateEducatorModal
        visible={showEditModal}
        user={editingItem}
        schools={schools}
        onCancel={() => setShowEditModal(false)}
        onFinish={onFinishModal}
      />
      <DeleteEducatorModal
        visible={showDeleteModal}
        user={deletingItem}
        onCancel={() => setShowDeleteModal(false)}
        onFinish={onFinishModal}
      />
    </>
  )
}

export default EducatorsTable
