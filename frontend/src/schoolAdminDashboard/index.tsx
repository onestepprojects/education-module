// eslint-disable-next-line no-use-before-define
import React, { FC, useEffect, useState } from 'react'

import { Divider } from 'antd'
import {} from '@ant-design/icons'

import 'antd/dist/antd.css'

import '../components/components.css'
import Banner from '../components/Banner'
import UserPanel from '../components/UserPanel'
import HeaderPanel from '../components/HeaderPanel'
import FooterBar from '../components/FooterBar'

import StudentsTable from './StudentsTable'
import EducatorsTable from './EducatorsTable'
import CourseTable from './CourseTable'

interface SchoolAdminDashboardProps {
  user: object
}

const SchoolAdminDashboard: FC<SchoolAdminDashboardProps> = ({ user }) => {
  return (
    <div className='dashboard-background-container'>
      <Banner user={user} />
      <div className='dashboard-container'>
        <HeaderPanel user={user} />
        <UserPanel user={user} noModalButtons={true} />
        <div>
          <CourseTable user={user} />
          <EducatorsTable user={user} />
          <StudentsTable user={user} />
        </div>
      </div>
      <FooterBar />
    </div>
  )
}

export default SchoolAdminDashboard
