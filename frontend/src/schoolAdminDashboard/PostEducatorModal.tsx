import React, { useState, useEffect } from 'react'
import { Button, Modal, Form, Input, Radio } from 'antd'
import PostEducatorForm from './PostEducatorForm'

import usersApi from '../api/users'

interface PostEducatorModalProps {
  visible: boolean
  schools: any[]
  onCancel: () => void
  onFinish: (any?) => void
}

const PostEducatorModal: React.FC<PostEducatorModalProps> = ({
  visible,
  schools,
  onCancel,
  onFinish,
}) => {
  const [form] = Form.useForm()
  const [loading, setLoading] = React.useState(false)
  const [persons, setPersons] = useState([])

  useEffect(() => {
    usersApi
      .getRegisteredUsers(false)
      .then(setPersons)
      .catch((e) => {
        console.error(`[PostEducatorModal] error loading persons API ${e}`)
      })
  }, [])

  const handleOk = async () => {
    setLoading(true)
    try {
      const values = await form.validateFields()
      console.log('[PostEducatorModal] form validate success:', values)

      const payload = {
        id: values.userID,
        name: values.username,
        email: values.email,
        role: values.role,
        schoolId: values.school,
      }

      let result = await usersApi.create(payload)
      console.log('[PostEducatorModal] created user:', result)

      // Log the URL to confirm user registration with the ORG service
      const updatePersonRoleUrl = `${process.env.REACT_APP_ORG_MODULE_API_URL}/composite/update-person-role-status/${payload.role}/${payload.schoolId}/${payload.id}/ACTIVE`
      console.log(`[PostEducatorModal] URL to confirm user registration: ${updatePersonRoleUrl}`)

      form.resetFields()
      onFinish()
    } catch (e) {
      console.log('[PostEducatorModal] submit error:', e)
      if (e instanceof Error) alert(e)
    } finally {
      setLoading(false)
    }
  }

  return (
    <Modal
      width={750}
      visible={visible}
      title='REGISTER USER'
      onCancel={onCancel}
      footer={[
        <Button key='back' onClick={onCancel}>
          Cancel
        </Button>,
        <Button key='submit' type='primary' loading={loading} onClick={handleOk}>
          Submit
        </Button>,
      ]}
    >
      <PostEducatorForm form={form} schools={schools} persons={persons} />
    </Modal>
  )
}

export default PostEducatorModal
