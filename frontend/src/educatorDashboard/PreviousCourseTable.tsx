import React, { FC, useEffect, useState } from 'react'
import { Table, Avatar, Divider, Button, Tooltip } from 'antd'
import {
  InfoCircleOutlined,
  UserOutlined,
  UsergroupAddOutlined,
  EditOutlined,
  DeleteOutlined,
} from '@ant-design/icons'

import coursesApi from '../api/courses'
import CanvasCourseButton from '../components/CanvasCourseButton'

const courseColumns = [
  {
    title: () => (
      <a>
        <InfoCircleOutlined className='padded' />
      </a>
    ),
    dataIndex: '',
    key: 'x',
    render: () => (
      <a>
        <Avatar size={20} icon={<UserOutlined />} />
      </a>
    ),
  },
  {
    title: 'SECTION ID',
    dataIndex: 'id',
    filters: [],
    // specify the condition of filtering result
    // here is that finding the name started with `value`
    onFilter: (value, record) => record.id.indexOf(value) === 0,
    sorter: (a, b) => a.id.length - b.id.length,
    sortDirections: ['descend'],
  },
  {
    title: 'COURSE ID',
    dataIndex: 'course_code',
    defaultSortOrder: 'descend',
    sorter: (a, b) => a.course_code - b.course_code,
  },
  {
    title: 'NAME',
    dataIndex: 'name',
    defaultSortOrder: 'descend',
    sorter: (a, b) => a.name - b.name,
  },
  {
    title: 'ENROLLMENT',
    dataIndex: 'enrollments',
    render: (enrollments) => <> {enrollments.length} </>,
    defaultSortOrder: 'descend',
    sorter: (a, b) => a.enrollments.length - b.enrollments.length,
  },
  {
    title: 'STATE',
    dataIndex: 'workflow_state',
    defaultSortOrder: 'descend',
    sorter: (a, b) => a.workflow_state - b.workflow_state,
  },
  {
    title: 'CANVAS',
    dataIndex: '',
    key: 'canvas',
    render: (id) => (
      <td className='user-container-icon-holder' rowSpan='2'>
        <CanvasCourseButton user={{ role: 'Educator' }} course={id} />
      </td>
    ),
  },
]

interface PreviousCourseTableProps {
  user: object
}

const PreviousCourseTable: FC<PreviousCourseTableProps> = ({ user }) => {
  const [loading, setLoading] = useState(false)
  const [data, setData] = useState([])
  const [lastUpdate, setLastUpdate] = useState(Date.now())

  const [showAddModal, setShowAddModal] = useState(false)
  const [showEditModal, setShowEditModal] = useState(false)
  const [showDeleteModal, setShowDeleteModal] = useState(false)
  const [editingItem, setEditingItem] = useState()
  const [deletingItem, setDeletingItem] = useState()

  useEffect(() => {
    setLoading(true)
    coursesApi
      .getUserCoursesByID(user.uuid)
      .then((courses) =>
        courses.filter(
          (c) => !(c.end_at == null || (c.end_at != null && new Date(c.end_at) <= new Date()))
        )
      )
      .then(setData)
      .catch((e) => console.error(`[CurrentCourseTable] error loading schools API ${e}`))
      .finally(() => setLoading(false))
  }, [lastUpdate])

  const onEdit = (item) => {
    setEditingItem(item)
    setShowEditModal(true)
  }
  const onDelete = (item) => {
    setDeletingItem(item)
    setShowDeleteModal(true)
  }

  const onFinishModal = () => {
    setShowAddModal(false)
    setShowEditModal(false)
    setShowDeleteModal(false)
    setLastUpdate(Date.now())
  }

  return (
    <>
      <Divider className='user-role-container' orientation='left'>
        <span style={{ paddingRight: '1em' }}>Previous Courses</span>
        <Button
          shape='circle'
          icon={<UsergroupAddOutlined />}
          onClick={() => setShowAddModal(true)}
        />
      </Divider>
      <div className='user-container'>
        <Table columns={courseColumns} loading={loading} dataSource={data} />
      </div>
    </>
  )
}

export default PreviousCourseTable
