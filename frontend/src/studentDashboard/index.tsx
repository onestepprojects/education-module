// eslint-disable-next-line no-use-before-define
import React, { FC, useEffect, useState } from 'react'

import { Divider } from 'antd'
import {} from '@ant-design/icons'

import 'antd/dist/antd.css'

import '../components/components.css'
import Banner from '../components/Banner'
import UserPanel from '../components/UserPanel'
import HeaderPanel from '../components/HeaderPanel'
import FooterBar from '../components/FooterBar'

import CurrentCourseTable from './CurrentCourseTable'
import PreviousCourseTable from './PreviousCourseTable'

interface EducatorDashboardProps {
  user: object
}

const StudentDashboard: FC<StudentDashboardProps> = ({ user }) => {
  return (
    <div className='dashboard-background-container'>
      <Banner user={user} />
      <div className='dashboard-container'>
        <HeaderPanel user={user} />
        <UserPanel user={user} noModalButtons={true} />
        <div>
          <CurrentCourseTable user={user} />
          <PreviousCourseTable user={user} />
        </div>
      </div>
      <FooterBar />
    </div>
  )
}

export default StudentDashboard
