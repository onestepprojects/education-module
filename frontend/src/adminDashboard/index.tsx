// eslint-disable-next-line no-use-before-define
import React, { FC, useEffect, useState } from 'react'

import { Divider } from 'antd'
import {} from '@ant-design/icons'

import 'antd/dist/antd.css'

import '../components/components.css'
import Banner from '../components/Banner'
import UserPanel from '../components/UserPanel'
import HeaderPanel from '../components/HeaderPanel'
import FooterBar from '../components/FooterBar'

import schoolsApi from '../api/schools'
import { SCHOOLS } from '../api/mockup'

import SchoolsTable from './SchoolsTable'
import UsersTable from './UsersTable'

interface AdminDashboardProps {
  user: object
}

const AdminDashboard: FC<AdminDashboardProps> = ({ user }) => {
  const [loadingSchool, setLoadingSchool] = useState(false)
  const [schools, setSchools] = useState([])
  const [lastUpdate, setLastUpdate] = useState(Date.now())

  useEffect(() => {
    setLoadingSchool(true)
    schoolsApi
      .getAll()
      .then((schools) => schools.sort((a, b) => a.name.localeCompare(b.name)))
      .then(setSchools)
      .catch((e) => {
        console.error(`[SchoolsTable] error loading schools API ${e}`)
        setSchools(SCHOOLS) // test mockup data
      })
      .finally(() => setLoadingSchool(false))
  }, [lastUpdate])

  return (
    <div className='dashboard-background-container'>
      <Banner user={user} />
      <div className='dashboard-container'>
        <HeaderPanel user={user} />
        <UserPanel user={user} noModalButtons={true} />
        <div>
          <SchoolsTable user={user} loading={loadingSchool} schools={schools} onSchoolsChanged={() => setLastUpdate(Date.now())} />
          <UsersTable user={user} schools={schools} />
        </div>
      </div>
      <FooterBar />
    </div>
  )
}

export default AdminDashboard
