import React, { FC, useEffect, useState } from 'react'
import { Table, Avatar, Divider, Button, Tooltip } from 'antd'
import { InfoCircleOutlined, UserOutlined, DeleteOutlined, PlusOutlined } from '@ant-design/icons'

import PostSchoolModal from './PostSchoolModal'
import DeleteSchoolModal from './DeleteSchoolModal'

const schoolColumns = [
  {
    title: () => (
      <a>
        <InfoCircleOutlined className='padded' />
      </a>
    ),
    dataIndex: '',
    key: 'x',
    render: () => (
      <a>
        <Avatar size={20} icon={<UserOutlined />} />
      </a>
    ),
  },
  {
    title: 'ID',
    dataIndex: 'id',
    filters: [],
    // specify the condition of filtering result
    // here is that finding the name started with `value`
    onFilter: (value, record) => record.id.indexOf(value) === 0,
    sorter: (a, b) => (a.id || '').localeCompare(b.id || ''),
  },
  {
    title: 'NAME',
    dataIndex: 'name',
    defaultSortOrder: 'ascend',
    sorter: (a, b) => (a.name || '').localeCompare(b.name || ''),
  },
  {
    title: 'STREET',
    dataIndex: ['address', 'address'],
    defaultSortOrder: 'ascend',
    sorter: (a, b) => (a.address.address || '').localeCompare(b.address.address || ''),
  },
  {
    title: 'CITY',
    dataIndex: ['address', 'city'],
    defaultSortOrder: 'ascend',
    sorter: (a, b) => (a.address.city || '').localeCompare(b.address.city || ''),
  },
  {
    title: 'STATE/ZIPCODE',
    dataIndex: ['address', 'state'],
    defaultSortOrder: 'ascend',
    sorter: (a, b) => (a.address.state || '').localeCompare(b.address.state || ''),
  },
  {
    title: 'COUNTRY',
    dataIndex: ['address', 'country'],
    defaultSortOrder: 'ascend',
    sorter: (a, b) => (a.address.country || '').localeCompare(b.address.country || ''),
  },
]

function s(table, onEdit, onDelete) {
  let hasSettings = false
  for (let i = 0; i < table.length; i++) {
    if (table[i].title == 'SETTINGS') {
      hasSettings = true
    }
  }
  if (!hasSettings) {
    table.push({
      title: 'SETTINGS',
      dataIndex: '',
      key: 'x',
      render: (text, record, index) => (
        <a>
          {/* <Button shape='circle' icon={<EditOutlined />} onClick={() => onEdit(record)} /> */}
          <Button shape='circle' icon={<DeleteOutlined />} onClick={() => onDelete(record)} />
        </a>
      ),
    })
  }
  return table
}

interface SchoolsTableProps {
  user: object
  loading: boolean
  schools: any[]
  onSchoolsChanged: (any?) => void
}

const SchoolsTable: FC<SchoolsTableProps> = ({ user, loading, schools, onSchoolsChanged }) => {

  const [showAddModal, setShowAddModal] = useState(false)
  const [showDeleteModal, setShowDeleteModal] = useState(false)
  const [deletingItem, setDeletingItem] = useState()

  const onDelete = (item) => {
    setDeletingItem(item)
    setShowDeleteModal(true)
  }

  const onFinishModal = () => {
    setShowAddModal(false)
    setShowDeleteModal(false)
    onSchoolsChanged()
  }

  return (
    <>
      <Divider className='user-role-container' orientation='left'>
        <span style={{ paddingRight: '1em' }}>Schools</span>
        <Button shape='circle' icon={<PlusOutlined />} onClick={() => setShowAddModal(true)} />
      </Divider>
      <div className='user-container'>
        <Table
          columns={s(schoolColumns, null, onDelete)}
          loading={loading} 
          dataSource={schools}
          pagination={{ pageSize: 10 }} />
      </div>
      <PostSchoolModal
        visible={showAddModal}
        onCancel={() => setShowAddModal(false)}
        onFinish={onFinishModal}
      />
      <DeleteSchoolModal
        visible={showDeleteModal}
        school={deletingItem}
        onCancel={() => setShowDeleteModal(false)}
        onFinish={onFinishModal}
      />
    </>
  )
}

export default SchoolsTable
