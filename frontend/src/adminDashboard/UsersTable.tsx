import React, { FC, useEffect, useState } from 'react'
import { Table, Avatar, Divider, Button, Tooltip } from 'antd'
import { InfoCircleOutlined, UserOutlined, DeleteOutlined, PlusOutlined } from '@ant-design/icons'

import PostUserModal from './PostUserModal'
import UpdateUserModal from './UpdateUserModal'
import DeleteUserModal from './DeleteUserModal'

import usersApi from '../api/users'

const userColumns = [
  {
    title: () => (
      <a>
        <InfoCircleOutlined className='padded' />
      </a>
    ),
    dataIndex: '',
    key: 'x',
    render: () => (
      <a>
        <Avatar size={20} icon={<UserOutlined />} />
      </a>
    ),
  },
  {
    title: 'ID',
    dataIndex: 'id',
    filters: [],
    // specify the condition of filtering result
    // here is that finding the name started with `value`
    onFilter: (value, record) => record.id.indexOf(value) === 0,
    sorter: (a, b) => (a.id || '').localeCompare(b.id || ''),
  },
  {
    title: 'NAME',
    dataIndex: 'name',
    defaultSortOrder: 'ascend',
    sorter: (a, b) => (a.name || '').localeCompare(b.name || ''),
  },
  {
    title: 'EMAIL',
    dataIndex: 'email',
    defaultSortOrder: 'ascend',
    sorter: (a, b) => (a.email || '').localeCompare(b.email || ''),
  },
  {
    title: 'ROLE',
    dataIndex: 'role',
    defaultSortOrder: 'ascend',
    sorter: (a, b) => (a.role || '').localeCompare(b.role || ''),
  },
  {
    title: 'SCHOOL',
    dataIndex: 'schoolId',
    defaultSortOrder: 'ascend',
    sorter: (a, b) => (a.schoolId || '').localeCompare(b.schoolId || ''),
  },
]

function s(table, onEdit, onDelete) {
  let hasSettings = false
  for (let i = 0; i < table.length; i++) {
    if (table[i].title == 'SETTINGS') {
      hasSettings = true
    }
  }
  if (!hasSettings) {
    table.push({
      title: 'SETTINGS',
      dataIndex: '',
      key: 'x',
      render: (text, record, index) => (
        <a>
          {/* <Button shape='circle' icon={<EditOutlined />} onClick={() => onEdit(record)} /> */}
          <Button shape='circle' icon={<DeleteOutlined />} onClick={() => onDelete(record)} />
        </a>
      ),
    })
  }
  return table
}

interface UsersTableProps {
  user: object
  schools: any[]
}

const UsersTable: FC<UsersTableProps> = ({ user, schools }) => {
  const [loading, setLoading] = useState(false)
  const [data, setData] = useState([])
  const [lastUpdate, setLastUpdate] = useState(Date.now())

  const [showAddModal, setShowAddModal] = useState(false)
  const [showEditModal, setShowEditModal] = useState(false)
  const [showDeleteModal, setShowDeleteModal] = useState(false)
  const [editingItem, setEditingItem] = useState()
  const [deletingItem, setDeletingItem] = useState()

  useEffect(() => {
    setLoading(true)
    usersApi
      .getRegisteredUsers(true)
      .then((persons) =>
        persons.map((p) => ({
          id: p.uuid,
          name: p.name,
          email: p.email,
          role: p.roles.includes('GLOBAL_ADMIN')
            ? 'Global Administrator'
            : p.roles.includes('ADMIN')
            ? 'School Administrator'
            : p.roles.includes('TEACHER')
            ? 'Educator'
            : 'Student',
          schoolId: p.school,
        }))
      )
      .then((people) => people.sort((a, b) => a.name.localeCompare(b.name)))
      .then(setData)
      .catch((e) => console.error(`[UsersTable] error loading users API ${e}`))
      .finally(() => setLoading(false))
  }, [lastUpdate])

  const onEdit = (item) => {
    setEditingItem(item)
    setShowEditModal(true)
  }
  const onDelete = (item) => {
    setDeletingItem(item)
    setShowDeleteModal(true)
  }

  const onFinishModal = () => {
    setShowAddModal(false)
    setShowEditModal(false)
    setShowDeleteModal(false)
    setLastUpdate(Date.now())
  }

  return (
    <>
      <Divider className='user-role-container' orientation='left'>
        <span style={{ paddingRight: '1em' }}>Users</span>
        <Button shape='circle' icon={<PlusOutlined />} onClick={() => setShowAddModal(true)} />
      </Divider>
      <div className='user-container'>
        <Table
          columns={s(userColumns, onEdit, onDelete)}
          loading={loading}
          dataSource={data}
          pagination={{ pageSize: 10 }}
        />
      </div>
      <PostUserModal
        visible={showAddModal}
        schools={schools}
        onCancel={() => setShowAddModal(false)}
        onFinish={onFinishModal}
      />
      <UpdateUserModal
        visible={showEditModal}
        user={editingItem}
        schools={schools}
        onCancel={() => setShowEditModal(false)}
        onFinish={onFinishModal}
      />
      <DeleteUserModal
        visible={showDeleteModal}
        user={deletingItem}
        onCancel={() => setShowDeleteModal(false)}
        onFinish={onFinishModal}
      />
    </>
  )
}

export default UsersTable
