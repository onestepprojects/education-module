import React, { useState } from 'react'
import { Button, Modal, Form } from 'antd'
import PostSchoolForm from './PostSchoolForm'
import schoolsApi from '../api/schools'

interface DeleteSchoolModalProps {
  visible: boolean
  school: any
  onCancel: () => void
  onFinish: (any?) => void
}

const DeleteSchoolModal: React.FC<DeleteSchoolModalProps> = ({
  visible,
  school,
  onCancel,
  onFinish,
}) => {
  const [form] = Form.useForm()
  const [loading, setLoading] = React.useState(false)

  const handleOk = async () => {
    setLoading(true)
    try {
      const values = await form.validateFields()
      console.log('[DeleteSchoolModal] form validate success:', values)

      const result = await schoolsApi.remove(school.id)
      console.log('[DeleteSchoolModal] deleted school:', result)
      form.resetFields()
      onFinish()
    } catch (e) {
      console.log('[DeleteSchoolModal] submit error:', e)
      if (e instanceof Error) alert(e)
    } finally {
      setLoading(false)
    }
  }

  return (
    <Modal
      title='DELETE EXISTING SCHOOL'
      width={750}
      visible={visible}
      onCancel={onCancel}
      footer={[
        <Button key='back' onClick={onCancel}>
          Cancel
        </Button>,
        <Button key='submit' type='primary' loading={loading} onClick={handleOk}>
          Submit
        </Button>,
      ]}
    >
      <PostSchoolForm form={form} school={school} forDelete={true} />
    </Modal>
  )
}

export default DeleteSchoolModal
