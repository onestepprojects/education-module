import React, { useState } from 'react'
import { Button, Modal, Form, Input, Radio } from 'antd'
import PostSchoolForm from './PostSchoolForm'

import schoolsApi from '../api/schools'

interface UpdateSchoolModalProps {
  visible: boolean
  administrators: any[]
  school: any
  onCancel: () => void
  onFinish: (any?) => void
}

const UpdateSchoolModal: React.FC<UpdateSchoolModalProps> = ({
  visible,
  administrators,
  school,
  onCancel,
  onFinish,
}) => {
  const [form] = Form.useForm()
  const [loading, setLoading] = React.useState(false)

  const handleOk = async () => {
    setLoading(true)
    try {
      const values = await form.validateFields()
      console.log('[UpdateSchoolModal] form validate success:', values)

      const payload = {
        id: school.id,
        name: values.schoolName,
        address: {
          address: values.address,
          state: values.state,
          city: values.city,
          country: values.country,
        },
        phones: {
          phone: values.phone,
        },
        adminId: values.schoolAdministrator,
      }
      const result = await schoolsApi.update(payload)
      console.log('[UpdateSchoolModal] updated school:', result)
      onFinish()
    } catch (e) {
      console.log('[UpdateSchoolModal] submit error:', e)
      if (e instanceof Error) alert(e)
    } finally {
      setLoading(false)
    }
  }

  return (
    <Modal
      title='UPDATE EXISTING SCHOOL'
      width={750}
      visible={visible}
      onCancel={onCancel}
      footer={[
        <Button key='back' onClick={onCancel}>
          Cancel
        </Button>,
        <Button key='submit' type='primary' loading={loading} onClick={handleOk}>
          Submit
        </Button>,
      ]}
    >
      <PostSchoolForm form={form} school={school} administrators={administrators} />
    </Modal>
  )
}

export default UpdateSchoolModal
