import React, { useState } from 'react'
import { Button, Modal, Form } from 'antd'
import PostUserForm from './PostUserForm'
import usersApi from '../api/users'

interface DeleteUserModalProps {
  visible: boolean
  user: any
  onCancel: () => void
  onFinish: (any?) => void
}

const DeleteUserModal: React.FC<DeleteUserModalProps> = ({ visible, user, onCancel, onFinish }) => {
  const [form] = Form.useForm()
  const [loading, setLoading] = React.useState(false)

  const handleOk = async () => {
    setLoading(true)
    try {
      const values = await form.validateFields()
      console.log('[DeleteUserModal] form validate success:', values)

      const result = await usersApi.remove(user.id)
      console.log('[DeleteUserModal] updated user:', result)
      form.resetFields()
      onFinish()
    } catch (e) {
      console.log('[DeleteUserModal] submit error:', e)
      if (e instanceof Error) alert(e)
    } finally {
      setLoading(false)
    }
  }

  return (
    <Modal
      title='UNREGISTER EXISTING USER'
      width={750}
      visible={visible}
      onCancel={onCancel}
      footer={[
        <Button key='back' onClick={onCancel}>
          Cancel
        </Button>,
        <Button key='submit' type='primary' loading={loading} onClick={handleOk}>
          Submit
        </Button>,
      ]}
    >
      <PostUserForm form={form} user={user} forDelete={true} />
    </Modal>
  )
}

export default DeleteUserModal
