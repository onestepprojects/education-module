import React, { useState } from 'react'
import { Button, Modal, Form, Input, Radio } from 'antd'
import PostSchoolForm from './PostSchoolForm'

import schoolsApi from '../api/schools'

interface PostSchoolModalProps {
  visible: boolean
  onCancel: () => void
  onFinish: (any?) => void
}

const PostSchoolModal: React.FC<PostSchoolModalProps> = ({
  visible,
  onCancel,
  onFinish,
}) => {
  const [form] = Form.useForm()
  const [loading, setLoading] = React.useState(false)

  const handleOk = async () => {
    setLoading(true)
    try {
      const values = await form.validateFields()
      console.log('[PostSchoolModal] form validate success:', values)

      // Sample payload:
      // const payload = {
      //   "name": "Sample University",
      //   "address": {"address": "200 Harvard Square","state":"MA","city":"Boston"},
      //   "phones": {"sat_phone":"1111", "phone": "1111", "mobile":"1111"}
      // }
      const payload = {
        name: values.schoolName,
        address: {
          address: values.address,
          state: values.state,
          city: values.city,
          country: values.country,
        },
        phones: {
          phone: values.phone,
        },
      }
      const result = await schoolsApi.create(payload)
      console.log('[PostSchoolModal] created school:', result)
      form.resetFields()
      onFinish()
    } catch (e) {
      console.log('[PostSchoolModal] submit error:', e)
      if (e instanceof Error) alert(e)
    } finally {
      setLoading(false)
    }
  }

  return (
    <Modal
      title='ADD NEW SCHOOL'
      width={750}
      visible={visible}
      onCancel={onCancel}
      footer={[
        <Button key='back' onClick={onCancel}>
          Cancel
        </Button>,
        <Button key='submit' type='primary' loading={loading} onClick={handleOk}>
          Submit
        </Button>,
      ]}
    >
      <PostSchoolForm form={form} />
    </Modal>
  )
}

export default PostSchoolModal
