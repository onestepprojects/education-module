// eslint-disable-next-line no-use-before-define
import React, { FC, useEffect, useState } from 'react'
import { RouteComponentProps } from 'react-router-dom'
import { useAppDispatch, authSlice, type Person } from './store'

import 'antd/dist/antd.css'
import './components/components.css'
import Banner from './components/Banner'
import HeaderPanel from './components/HeaderPanel'
import UserPanel from './components/UserPanel'
import CanvasTable from './components/CanvasTable'
import FooterBar from './components/FooterBar'
import ModalButton from './components/ModalButton'
import ModalForm from './components/ModalForm'

import { authentication } from './api/auth'
import AdminDashboard from './adminDashboard'
import SchoolAdminDashboard from './schoolAdminDashboard'
import EducatorDashboard from './educatorDashboard'
import StudentDashboard from './studentDashboard'

import usersApi from './api/users'

export interface EducationContainerProps extends RouteComponentProps {
  auth: {
    token?: string
    user?: Person
    person?: Person
    roles?: string[]
  }
}

export const EducationContainer: FC<EducationContainerProps> = ({ auth }) => {
  const [user, setUser] = useState<Person>(auth.user || auth.person)
  const [loading, setLoading] = useState(false)
  const [lastUpdate, setLastUpdate] = useState(Date.now())

  const dispatch = useAppDispatch()

  useEffect(() => {
    if (!auth) return
    dispatch(authSlice.actions.setAuth(auth))
  }, [auth])

  useEffect(() => {
    if (auth && !user) {
      setUser(auth.user || auth.person)
    }
    if (user) {
      setLoading(true)
      usersApi
        .getUserByID(user.uuid)
        .then((results) => {
          let update = {}

          if (results.length == 1) {
            update.school = results[0]
            update.schoolId = results[0].name
            if (results[0].roles.includes('GLOBAL_ADMIN')) {
              update = Object.assign(update, user, { role: 'Global Administrator' })
            } else if (results[0].roles.includes('ADMIN')) {
              update = Object.assign(update, user, { role: 'School Administrator' })
            } else if (results[0].roles.includes('TEACHER')) {
              update = Object.assign(update, user, { role: 'Teacher' })
            } else if (results[0].roles.includes('STUDENT')) {
              update = Object.assign(update, user, { role: 'Student' })
            }
          } else {
            update = Object.assign(update, user, { role: 'Global Administrator' })
          }
          console.log(`Logged-in user:`, update)
          setUser(update)
        })
        .catch((e) => console.error(`[GetUser] error loading user from API ${e}`))
        .finally(() => setLoading(false))
    }
  }, [auth])

  if (user) {
    if (user && user.role == 'Global Administrator') {
      // Return the Admin Dashboard component for the user who has the role of 'Global Administrator'
      return <AdminDashboard user={user} />
    }

    if (user && user.role == 'School Administrator') {
      // Return the School Admin Dashboard component for the user who has the role of 'School Administrator'
      return <SchoolAdminDashboard user={user} />
    }

    if (user && user.role == 'Teacher') {
      // Return the Educator Dashboard component for the user who has the role of 'Educator'
      return <EducatorDashboard user={user} />
    }

    if (user && user.role == 'Student') {
      // Return the StudentDashboard component for the user who has the role of 'Student'
      return <StudentDashboard user={user} />
    }

    // Fallback in the event of a failure
    return (
      <div className='dashboard-background-container'>
        <Banner user={user} />
        <div className='dashboard-container'>
          <HeaderPanel user={user} />
          <UserPanel user={user} />
          <CanvasTable user={user} />
        </div>
        <FooterBar />
      </div>
    )
  } else {
    return (
      <div className='dashboard-background-container'>
        <Banner user={user} />
        <div className='dashboard-container'>
          <HeaderPanel user={user} />
          <UserPanel user={user} />
          <CanvasTable user={user} />
        </div>
        <FooterBar />
      </div>
    )
  }
}
