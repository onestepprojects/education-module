// eslint-disable-next-line no-use-before-define
import React, { FC, useEffect, useState } from 'react'
import { render } from 'react-dom'
import Amplify, { Auth, Hub } from 'aws-amplify'
import type { Person } from './store'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import App from './App'

import { authentication } from './api/auth'
import AdminDashboard from './adminDashboard'
import SchoolAdminDashboard from './schoolAdminDashboard'
import EducatorDashboard from './EducatorDashboard'
import StudentDashboard from './StudentDashboard'

import usersApi from './api/users'

Amplify.configure({
  userPoolId: process.env.REACT_APP_USER_POOL_ID,
  region: process.env.REACT_APP_USER_POOL_REGION,
  identityPoolRegion: process.env.REACT_APP_USER_POOL_IDENTITY_POOL_REGION,
  userPoolWebClientId: process.env.REACT_APP_USER_POOL_USER_POOL_WEB_CLIENT_ID,
  oauth: {
    domain: process.env.REACT_APP_USER_POOL_DOMAIN,
    scope: ['openid', 'profile'],
    redirectSignIn: process.env.REACT_APP_USER_POOL_REDIRECT_SIGN_IN,
    redirectSignOut: process.env.REACT_APP_USER_POOL_REDIRECT_SIGN_OUT,
    responseType: 'code',
  },
})

const getJwtToken = () =>
  Auth.currentAuthenticatedUser().then((user) =>
    user.getSignInUserSession().getAccessToken().getJwtToken()
  )

const Demo: FC = () => {
  const [token, setToken] = useState('')
  const [roles, setRoles] = useState([])
  const [user, setUser] = useState<Person>()

  useEffect(() => {
    getJwtToken()
      .then(setToken)
      .catch(() => Auth.federatedSignIn())
  }, [])

  useEffect(() => {
    if (!token) return

    fetch(
      `${
        process.env.REACT_APP_ORG_MODULE_API_URL || 'https://test.onesteprelief.org/onestep/org'
      }/persons/requester/token`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    )
      .then((res) => res.json())
      .then((data) => setUser(data))

    fetch(
      `${
        process.env.REACT_APP_AUTHORIZATION_API_URL ||
        'https://test.onesteprelief.org/onestep/authorization'
      }/user`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    )
      .then((res) => res.json())
      .then(({ data }) => setRoles(data.roles))

    const handler = ({ payload: { event } }) => {
      switch (event) {
        case 'signIn':
        case 'cognitoHostedUI':
          getJwtToken().then(setToken)
          break
        case 'signOut':
          setToken('')
          break
        case 'signIn_failure':
        case 'cognitoHostedUI_failure':
        default:
          break
      }
    }

    Hub.listen('auth', handler)

    return () => Hub.remove('auth', handler)
  }, [token])

  if (!token || !user || !roles) return null

  const auth = { token, user, roles }

  return (
    <BrowserRouter>
      <Switch>
        <Route
          path='/'
          render={(routerProps) => (
            <App {...routerProps} auth={auth} mountPath={routerProps.match.path} />
          )}
        />
      </Switch>
    </BrowserRouter>
  )
}

render(<Demo />, document.getElementById('root'))
