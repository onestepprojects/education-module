// eslint-disable-next-line no-use-before-define
import React, { FC } from 'react'
import { BrowserRouter } from 'react-router-dom'
import { Provider } from 'react-redux'
import { store } from './store'
import { EducationContainer, OneStepContainerProps } from './Container'

interface EducationModuleProps extends OneStepContainerProps {
  /** Module router base name. */
  mountPath: string
  /** @deprecated Use `auth` property instead. The auth logic should be handled centrally in the app. */
  authHelper?: any
  /** @deprecated Use `auth` property instead. The auth logic should be handled centrally in the app. */
  user?: any
}

const Education: FC<EducationModuleProps> = ({ mountPath, ...restProps }) => {
  return (
    <BrowserRouter basename={mountPath}>
      <Provider store={store}>
        <EducationContainer {...restProps} />
      </Provider>
    </BrowserRouter>
  )
}

export default Education
