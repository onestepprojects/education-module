import React, { FC } from 'react'
import { Avatar, Button } from 'antd'
import {
  UserOutlined,
  SolutionOutlined,
  SettingOutlined,
  UsergroupAddOutlined,
  TeamOutlined,
  UsergroupDeleteOutlined,
  UserAddOutlined,
  UserSwitchOutlined,
  UserDeleteOutlined,
} from '@ant-design/icons'

import ModalButton from './ModalButton'
import CanvasDashboardButton from './CanvasDashboardButton'

/* WILL NEED PROPS*/
const UserPanel: FC = (props) => {
  const user = props.user ? props.user : null
  const helloUser = props.user ? 'Hello, ' + props.user.name : 'Hello!'
  const currentRole = props.user ? props.user.role : ''
  const schools = props.schools
  const administrators = props.administrators
  const noModalButtons = props.noModalButtons

  return (
    <div className='user-container'>
      <table className='user-container-table'>
        <tbody>
          <tr>
            <td className='user-container-icon-holder'>
              <>
                <div className='user-element-container'>
                  <Avatar size={64} icon={<UserOutlined />} />
                </div>
              </>
            </td>
            <td>
              <table className='user-container-table'>
                <tbody>
                  <tr>
                    <td>
                      <div className='user-element-header-container'>
                        <h3 className='user-element-header'>
                          {' '}
                          <div className='onestep-tiny-header-fix'>{helloUser}</div>{' '}
                        </h3>
                      </div>
                    </td>
                    <td className='user-container-icon-holder' rowSpan='2'>
                      <CanvasDashboardButton user={user} />
                    </td>
                    <td className='spacing'></td>
                    <td>
                      {noModalButtons || (
                        <div className='clickable-icon'>
                          <ModalButton icon='SolutionOutlined' user={user} />
                        </div>
                      )}
                    </td>
                    <td>
                      {noModalButtons || (
                        <div className='clickable-icon'>
                          <ModalButton
                            icon='UsergroupAddOutlined'
                            user={user}
                            administrators={administrators}
                          />
                        </div>
                      )}
                    </td>
                    <td>
                      {noModalButtons || (
                        <div className='clickable-icon'>
                          <ModalButton
                            icon='TeamOutlined'
                            user={user}
                            administrators={administrators}
                          />
                        </div>
                      )}
                    </td>
                    <td>
                      {noModalButtons || (
                        <div className='clickable-icon'>
                          <ModalButton
                            icon='UsergroupDeleteOutlined'
                            user={user}
                            administrators={administrators}
                          />
                        </div>
                      )}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <div className='user-role-container'>
                        <p> {currentRole} </p>
                      </div>
                    </td>
                    <td></td>
                    <td>
                      {noModalButtons || (
                        <div className='clickable-icon'>
                          <ModalButton icon='SettingOutlined' user={user} />
                        </div>
                      )}
                    </td>
                    <td>
                      {noModalButtons || (
                        <div className='clickable-icon'>
                          <ModalButton icon='UserAddOutlined' user={user} schools={schools} />
                        </div>
                      )}
                    </td>
                    <td>
                      {noModalButtons || (
                        <div className='clickable-icon'>
                          <ModalButton icon='UserSwitchOutlined' user={user} schools={schools} />
                        </div>
                      )}
                    </td>
                    <td>
                      {noModalButtons || (
                        <div className='clickable-icon'>
                          <ModalButton icon='UserDeleteOutlined' user={user} schools={schools} />
                        </div>
                      )}
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  )
}

export default UserPanel
