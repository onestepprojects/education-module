import React, { FC } from 'react'
import { Button } from 'antd'
import { MenuOutlined } from '@ant-design/icons'
import LoginButton from './LoginButton'

/* 
The Banner operates as the header of the page. It states who is signed in
and allows the user to sign in, sign out, and registor, in addition to opening
and closing the sidebar
*/

const Banner: FC = (props) => {
  // Display current signed in user
  const details = props.user ? 'Signed in as ' + props.user.name : 'No one is signed in'
  const signedInFlag = props.user ? 'Sign Out' : 'Log In'
  const setupFlag = 'Register'

  return (
    <div className='header-banner'>
      <table className='banner-data-container'>
        <tbody>
          <tr>
            <td>
              <div className='clickable-banner-icon'>
                <MenuOutlined />
              </div>
            </td>
            <td>
              <div className='banner-element-header-container'>
                <h3 className='header-thin'>
                  <div className='onestep-tiny-header-fix'>Education Module</div>
                </h3>
              </div>
            </td>
            <td>
              <div className='banner-element-header-container'>
                <p className='banner-element-quote-container'> {details} </p>
              </div>
            </td>
            <td className='spacing'></td>
            <td>
              <LoginButton type={signedInFlag} />
            </td>
            <td className='clickable-banner-icon'>
              <LoginButton type={setupFlag} />
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  )
}

export default Banner
