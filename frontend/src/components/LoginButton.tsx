import React, { FC } from 'react'
import { Button } from 'antd'
import { MenuOutlined } from '@ant-design/icons'
import { signOut } from '../api/auth'

/*
This class sets the type of log in buttons available to the user based on their signed-in status
*/
const LoginButton: FC = (props) => {
  if (props.type == 'Log In') {
    return (
      <div>
        <Button type='primary'>Log In</Button>
      </div>
    )
  } else if (props.type == 'Sign Out') {
    return (
      <div>
        <Button type='danger' className='redirect-button' onClick={signOut}>
          Sign Out
        </Button>
      </div>
    )
  } else {
    return (
      <div>
        <Button>Register</Button>
      </div>
    )
  }
}

export default LoginButton
