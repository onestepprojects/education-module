import React, { FC } from 'react'
import { Form, Input, Button, Checkbox, Select, Option, Cascader } from 'antd'
import { Typography, Space } from 'antd'
const { Text, Link } = Typography
import {} from '@ant-design/icons'

const AccountSettingsForm: FC = (props) => {
  const onFinish = (values) => {
    console.log('Success:', values)
  }

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo)
  }

  if (props.type == 'update') {
    return (
      <Form
        name='basic'
        labelCol={{
          span: 5,
        }}
        wrapperCol={{
          span: 16,
        }}
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete='off'
      >
        <Form.Item
          label='USER ID'
          name='userID'
          rules={[
            {
              required: true,
              message: 'A new ID is being generated!',
            },
          ]}
        >
          <Input disabled={true} placeholder={'0000-0000-0000-0000'} />
        </Form.Item>

        <Form.Item
          label='USERNAME'
          name='username'
          rules={[
            {
              required: true,
              message: 'Please input name of your new school!',
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label='EMAIL'
          name='email'
          rules={[
            {
              required: true,
              message: 'Please input name of your new school!',
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label='ROLE'
          name='role'
          rules={[
            {
              required: true,
              message: 'Please input name of your new school!',
            },
          ]}
        >
          <Select disabled={true} style={{ width: '100%' }}>
            <Input />
            <Cascader style={{ width: '70%' }} />
          </Select>
        </Form.Item>

        <Form.Item
          label='SCHOOL'
          name='school'
          rules={[
            {
              required: true,
              message: 'Please input name of your new school!',
            },
          ]}
        >
          <Select style={{ width: '100%' }}>
            <Input />
            <Cascader style={{ width: '70%' }} />
          </Select>
        </Form.Item>
      </Form>
    )
  } else {
    return (
      <Form
        name='basic'
        labelCol={{
          span: 5,
        }}
        wrapperCol={{
          span: 16,
        }}
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete='off'
      >
        <Form.Item
          label='USER ID'
          name='userID'
          rules={[
            {
              required: true,
              message: 'A new ID is being generated!',
            },
          ]}
        >
          <Input disabled={true} placeholder={'0000-0000-0000-0000'} />
        </Form.Item>

        <Form.Item
          label='USERNAME'
          name='username'
          rules={[
            {
              required: true,
              message: 'Please input name of your new school!',
            },
          ]}
        >
          <Input disabled={true} />
        </Form.Item>

        <Form.Item
          label='EMAIL'
          name='email'
          rules={[
            {
              required: true,
              message: 'Please input name of your new school!',
            },
          ]}
        >
          <Input disabled={true} />
        </Form.Item>

        <Form.Item
          label='ROLE'
          name='role'
          rules={[
            {
              required: true,
              message: 'Please input name of your new school!',
            },
          ]}
        >
          <Select disabled={true} style={{ width: '100%' }}>
            <Input />
            <Cascader style={{ width: '70%' }} />
          </Select>
        </Form.Item>

        <Form.Item
          label='SCHOOL'
          name='school'
          rules={[
            {
              required: true,
              message: 'Please input name of your new school!',
            },
          ]}
        >
          <Select disabled={true} style={{ width: '100%' }}>
            <Input />
            <Cascader style={{ width: '70%' }} />
          </Select>
        </Form.Item>
      </Form>
    )
  }
}

export default AccountSettingsForm
