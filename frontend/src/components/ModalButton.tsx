import React, { FC } from 'react'
import { Modal, Button, Tooltip } from 'antd'
import {
  UserOutlined,
  SolutionOutlined,
  SettingOutlined,
  UsergroupAddOutlined,
  TeamOutlined,
  UsergroupDeleteOutlined,
  UserAddOutlined,
  UserSwitchOutlined,
  UserDeleteOutlined,
} from '@ant-design/icons'

import ModalForm from './ModalForm'
import AccountSettingsForm from './AccountSettingsForm'
import PostSchoolForm from './PostSchoolForm'
import PostUserForm from './PostUserForm'
//--------

/* WILL NEED PROPS*/
const ModalButton: FC = (props) => {
  const role = props.user && props.user.role ? props.user.role : null
  return (
    <ModalSelector
      icon={props.icon}
      role={role}
      schools={props.schools}
      administrators={props.administrators}
    />
  )
}

class ModalSelector extends React.Component {
  state = {
    loading: false,
    visible: false,
  }

  showModal = () => {
    this.setState({
      visible: true,
    })
  }

  handleOk = () => {
    this.setState({ loading: true })
    setTimeout(() => {
      this.setState({ loading: false, visible: false })
    }, 3000)
  }

  handleCancel = () => {
    this.setState({ visible: false })
  }

  render() {
    const { visible, loading } = this.state

    if (this.props.role == 'Global Administrator') {
      if (this.props.icon == 'SolutionOutlined') {
        return (
          <>
            <Tooltip title='My Information'>
              <Button shape='circle' icon={<SolutionOutlined />} onClick={this.showModal} />
              <Modal
                visible={visible}
                title='MY INFORMATION'
                onOk={this.handleOk}
                onCancel={this.handleCancel}
                footer={[
                  <Button key='back' onClick={this.handleCancel}>
                    Cancel
                  </Button>,
                  <Button key='submit' type='primary' loading={loading} onClick={this.handleOk}>
                    Submit
                  </Button>,
                ]}
              >
                <AccountSettingsForm type='view' />
              </Modal>
            </Tooltip>
          </>
        )
      } else if (this.props.icon == 'SettingOutlined') {
        return (
          <>
            <Tooltip title='Account Settings'>
              <Button shape='circle' icon={<SettingOutlined />} onClick={this.showModal} />
              <Modal
                visible={visible}
                title='ACCOUNT SETTINGS'
                onOk={this.handleOk}
                onCancel={this.handleCancel}
                footer={[
                  <Button key='back' onClick={this.handleCancel}>
                    Cancel
                  </Button>,
                  <Button key='submit' type='primary' loading={loading} onClick={this.handleOk}>
                    Submit
                  </Button>,
                ]}
              >
                <AccountSettingsForm type='update' />
              </Modal>
            </Tooltip>
          </>
        )
      } else if (this.props.icon == 'UsergroupAddOutlined') {
        return (
          <>
            <Tooltip title='Add New School'>
              <Button shape='circle' icon={<UsergroupAddOutlined />} onClick={this.showModal} />
              <Modal
                width={750}
                visible={visible}
                title='ADD NEW SCHOOL'
                onOk={this.handleOk}
                onCancel={this.handleCancel}
                footer={[
                  <Button key='back' onClick={this.handleCancel}>
                    Cancel
                  </Button>,
                  <Button key='submit' type='primary' loading={loading} onClick={this.handleOk}>
                    Submit
                  </Button>,
                ]}
              >
                <PostSchoolForm type='add' administrators={this.props.administrators} />
              </Modal>
            </Tooltip>
          </>
        )
      } else if (this.props.icon == 'UserAddOutlined') {
        return (
          <>
            <Tooltip title='Add New User'>
              <Button shape='circle' icon={<UserAddOutlined />} onClick={this.showModal} />
              <Modal
                width={750}
                visible={visible}
                title='ADD NEW USER'
                onOk={this.handleOk}
                onCancel={this.handleCancel}
                footer={[
                  <Button key='back' onClick={this.handleCancel}>
                    Cancel
                  </Button>,
                  <Button key='submit' type='primary' loading={loading} onClick={this.handleOk}>
                    Submit
                  </Button>,
                ]}
              >
                <PostUserForm type='add' schools={this.props.schools} />
              </Modal>
            </Tooltip>
          </>
        )
      } else if (this.props.icon == 'TeamOutlined') {
        return (
          <>
            <Tooltip title='Update Existing School'>
              <Button shape='circle' icon={<TeamOutlined />} onClick={this.showModal} />
              <Modal
                width={750}
                visible={visible}
                title='UPDATE EXISTING SCHOOL'
                onOk={this.handleOk}
                onCancel={this.handleCancel}
                footer={[
                  <Button key='back' onClick={this.handleCancel}>
                    Cancel
                  </Button>,
                  <Button key='submit' type='primary' loading={loading} onClick={this.handleOk}>
                    Submit
                  </Button>,
                ]}
              >
                <PostSchoolForm type='update' administrators={this.props.administrators} />
              </Modal>
            </Tooltip>
          </>
        )
      } else if (this.props.icon == 'UserSwitchOutlined') {
        return (
          <>
            <Tooltip title='Update Existing User'>
              <Button shape='circle' icon={<UserSwitchOutlined />} onClick={this.showModal} />
              <Modal
                width={750}
                visible={visible}
                title='UPDATE EXISTING USER'
                onOk={this.handleOk}
                onCancel={this.handleCancel}
                footer={[
                  <Button key='back' onClick={this.handleCancel}>
                    Cancel
                  </Button>,
                  <Button key='submit' type='primary' loading={loading} onClick={this.handleOk}>
                    Submit
                  </Button>,
                ]}
              >
                <PostUserForm type='update' schools={this.props.schools} />
              </Modal>
            </Tooltip>
          </>
        )
      } else if (this.props.icon == 'UsergroupDeleteOutlined') {
        return (
          <>
            <Tooltip title='Remove School'>
              <Button shape='circle' icon={<UsergroupDeleteOutlined />} onClick={this.showModal} />
              <Modal
                width={750}
                visible={visible}
                title='REMOVE SCHOOL'
                onOk={this.handleOk}
                onCancel={this.handleCancel}
                footer={[
                  <Button key='back' onClick={this.handleCancel}>
                    Cancel
                  </Button>,
                  <Button key='submit' type='primary' loading={loading} onClick={this.handleOk}>
                    Submit
                  </Button>,
                ]}
              >
                <PostSchoolForm
                  type='delete'
                  forDelete={true}
                  administrators={this.props.administrators}
                />
              </Modal>
            </Tooltip>
          </>
        )
      } else if (this.props.icon == 'UserDeleteOutlined') {
        return (
          <>
            <Tooltip title='Remove User'>
              <Button shape='circle' icon={<UserDeleteOutlined />} onClick={this.showModal} />
              <Modal
                width={750}
                visible={visible}
                title='REMOVE USER'
                onOk={this.handleOk}
                onCancel={this.handleCancel}
                footer={[
                  <Button key='back' onClick={this.handleCancel}>
                    Cancel
                  </Button>,
                  <Button key='submit' type='primary' loading={loading} onClick={this.handleOk}>
                    Submit
                  </Button>,
                ]}
              >
                <PostUserForm type='delete' schools={this.props.schools} forDelete={true} />
              </Modal>
            </Tooltip>
          </>
        )
      } else {
        return <></>
      }
    } else if (this.props.role == 'School Administrator') {
      if (this.props.icon == 'SolutionOutlined') {
        return (
          <>
            <Tooltip title='My Information'>
              <Button shape='circle' icon={<SolutionOutlined />} onClick={this.showModal} />
              <Modal
                visible={visible}
                title='MY INFORMATION'
                onOk={this.handleOk}
                onCancel={this.handleCancel}
                footer={[
                  <Button key='back' onClick={this.handleCancel}>
                    Cancel
                  </Button>,
                  <Button key='submit' type='primary' loading={loading} onClick={this.handleOk}>
                    Submit
                  </Button>,
                ]}
              >
                <AccountSettingsForm type='view' />
              </Modal>
            </Tooltip>
          </>
        )
      } else if (this.props.icon == 'SettingOutlined') {
        return (
          <>
            <Tooltip title='Account Settings'>
              <Button shape='circle' icon={<SettingOutlined />} onClick={this.showModal} />
              <Modal
                visible={visible}
                title='ACCOUNT SETTINGS'
                onOk={this.handleOk}
                onCancel={this.handleCancel}
                footer={[
                  <Button key='back' onClick={this.handleCancel}>
                    Cancel
                  </Button>,
                  <Button key='submit' type='primary' loading={loading} onClick={this.handleOk}>
                    Submit
                  </Button>,
                ]}
              >
                <AccountSettingsForm type='update' />
              </Modal>
            </Tooltip>
          </>
        )
      } else if (this.props.icon == 'UsergroupAddOutlined') {
        return (
          <>
            <Tooltip title='Add New Educator'>
              <Button shape='circle' icon={<UsergroupAddOutlined />} onClick={this.showModal} />
              <Modal
                visible={visible}
                title='ADD NEW TEACHER'
                onOk={this.handleOk}
                onCancel={this.handleCancel}
                footer={[
                  <Button key='back' onClick={this.handleCancel}>
                    Cancel
                  </Button>,
                  <Button key='submit' type='primary' loading={loading} onClick={this.handleOk}>
                    Submit
                  </Button>,
                ]}
              >
                <PostUserForm type='add' limit='Educator' schools={this.props.schools} />
              </Modal>
            </Tooltip>
          </>
        )
      } else if (this.props.icon == 'UserAddOutlined') {
        return (
          <>
            <Tooltip title='Add New Student'>
              <Button shape='circle' icon={<UserAddOutlined />} onClick={this.showModal} />
              <Modal
                visible={visible}
                title='ADD NEW STUDENT'
                onOk={this.handleOk}
                onCancel={this.handleCancel}
                footer={[
                  <Button key='back' onClick={this.handleCancel}>
                    Cancel
                  </Button>,
                  <Button key='submit' type='primary' loading={loading} onClick={this.handleOk}>
                    Submit
                  </Button>,
                ]}
              >
                <PostUserForm type='add' limit='Student' schools={this.props.schools} />
              </Modal>
            </Tooltip>
          </>
        )
      } else if (this.props.icon == 'TeamOutlined') {
        return (
          <>
            <Tooltip title='Update Existing Educator'>
              <Button shape='circle' icon={<TeamOutlined />} onClick={this.showModal} />
              <Modal
                visible={visible}
                title='UPDATE EXISTING TEACHER'
                onOk={this.handleOk}
                onCancel={this.handleCancel}
                footer={[
                  <Button key='back' onClick={this.handleCancel}>
                    Cancel
                  </Button>,
                  <Button key='submit' type='primary' loading={loading} onClick={this.handleOk}>
                    Submit
                  </Button>,
                ]}
              >
                <PostUserForm type='update' limit='Educator' schools={this.props.schools} />
              </Modal>
            </Tooltip>
          </>
        )
      } else if (this.props.icon == 'UserSwitchOutlined') {
        return (
          <>
            <Tooltip title='Update Existing Student'>
              <Button shape='circle' icon={<UserSwitchOutlined />} onClick={this.showModal} />
              <Modal
                visible={visible}
                title='UPDATE EXISTING STUDENT'
                onOk={this.handleOk}
                onCancel={this.handleCancel}
                footer={[
                  <Button key='back' onClick={this.handleCancel}>
                    Cancel
                  </Button>,
                  <Button key='submit' type='primary' loading={loading} onClick={this.handleOk}>
                    Submit
                  </Button>,
                ]}
              >
                <PostUserForm type='update' limit='Student' schools={this.props.schools} />
              </Modal>
            </Tooltip>
          </>
        )
      } else if (this.props.icon == 'UsergroupDeleteOutlined') {
        return (
          <>
            <Tooltip title='Delete Educator'>
              <Button shape='circle' icon={<UsergroupDeleteOutlined />} onClick={this.showModal} />
              <Modal
                visible={visible}
                title='DELETE TEACHER'
                onOk={this.handleOk}
                onCancel={this.handleCancel}
                footer={[
                  <Button key='back' onClick={this.handleCancel}>
                    Cancel
                  </Button>,
                  <Button key='submit' type='primary' loading={loading} onClick={this.handleOk}>
                    Submit
                  </Button>,
                ]}
              >
                <PostUserForm
                  type='delete'
                  limit='Educator'
                  schools={this.props.schools}
                  forDelete={true}
                />
              </Modal>
            </Tooltip>
          </>
        )
      } else if (this.props.icon == 'UserDeleteOutlined') {
        return (
          <>
            <Tooltip title='Delete Student'>
              <Button shape='circle' icon={<UserDeleteOutlined />} onClick={this.showModal} />
              <Modal
                visible={visible}
                title='DELETE STUDENT'
                onOk={this.handleOk}
                onCancel={this.handleCancel}
                footer={[
                  <Button key='back' onClick={this.handleCancel}>
                    Cancel
                  </Button>,
                  <Button key='submit' type='primary' loading={loading} onClick={this.handleOk}>
                    Submit
                  </Button>,
                ]}
              >
                <PostUserForm
                  type='delete'
                  limit='Student'
                  schools={this.props.schools}
                  forDelete={true}
                />
              </Modal>
            </Tooltip>
          </>
        )
      } else {
        return <></>
      }
    } else if (this.props.role == 'Educator') {
      if (this.props.icon == 'SolutionOutlined') {
        return (
          <>
            <Tooltip title='My Information'>
              <Button shape='circle' icon={<SolutionOutlined />} onClick={this.showModal} />
              <Modal
                visible={visible}
                title='MY INFORMATION'
                onOk={this.handleOk}
                onCancel={this.handleCancel}
                footer={[
                  <Button key='back' onClick={this.handleCancel}>
                    Cancel
                  </Button>,
                  <Button key='submit' type='primary' loading={loading} onClick={this.handleOk}>
                    Submit
                  </Button>,
                ]}
              >
                <AccountSettingsForm type='view' />
              </Modal>
            </Tooltip>
          </>
        )
      } else if (this.props.icon == 'SettingOutlined') {
        return (
          <>
            <Tooltip title='Account Settings'>
              <Button shape='circle' icon={<SettingOutlined />} onClick={this.showModal} />
              <Modal
                visible={visible}
                title='ACCOUNT SETTINGS'
                onOk={this.handleOk}
                onCancel={this.handleCancel}
                footer={[
                  <Button key='back' onClick={this.handleCancel}>
                    Cancel
                  </Button>,
                  <Button key='submit' type='primary' loading={loading} onClick={this.handleOk}>
                    Submit
                  </Button>,
                ]}
              >
                <AccountSettingsForm type='update' />
              </Modal>
            </Tooltip>
          </>
        )
      } else if (this.props.icon == 'UsergroupAddOutlined') {
        return (
          <>
            <Tooltip title='Invite Educator'>
              <Button shape='circle' icon={<UsergroupAddOutlined />} onClick={this.showModal} />
              <Modal
                visible={visible}
                title='INVITE TEACHER'
                onOk={this.handleOk}
                onCancel={this.handleCancel}
                footer={[
                  <Button key='back' onClick={this.handleCancel}>
                    Cancel
                  </Button>,
                  <Button key='submit' type='primary' loading={loading} onClick={this.handleOk}>
                    Submit
                  </Button>,
                ]}
              >
                <PostUserForm type='add' limit='Educator' schools={this.props.schools} />
              </Modal>
            </Tooltip>
          </>
        )
      } else if (this.props.icon == 'UserAddOutlined') {
        return (
          <>
            <Tooltip title='Invite Student'>
              <Button shape='circle' icon={<UserAddOutlined />} onClick={this.showModal} />
              <Modal
                visible={visible}
                title='INVITE STUDENT'
                onOk={this.handleOk}
                onCancel={this.handleCancel}
                footer={[
                  <Button key='back' onClick={this.handleCancel}>
                    Cancel
                  </Button>,
                  <Button key='submit' type='primary' loading={loading} onClick={this.handleOk}>
                    Submit
                  </Button>,
                ]}
              >
                <PostUserForm type='add' limit='Student' schools={this.props.schools} />
              </Modal>
            </Tooltip>
          </>
        )
      } else {
        return <></>
      }
    } else if (this.props.role == 'Student') {
      if (this.props.icon == 'SolutionOutlined') {
        return (
          <>
            <Tooltip title='My Information'>
              <Button shape='circle' icon={<SolutionOutlined />} onClick={this.showModal} />
              <Modal
                visible={visible}
                title='MY INFORMATION'
                onOk={this.handleOk}
                onCancel={this.handleCancel}
                footer={[
                  <Button key='back' onClick={this.handleCancel}>
                    Cancel
                  </Button>,
                  <Button key='submit' type='primary' loading={loading} onClick={this.handleOk}>
                    Submit
                  </Button>,
                ]}
              >
                <AccountSettingsForm type='view' />
              </Modal>
            </Tooltip>
          </>
        )
      } else if (this.props.icon == 'SettingOutlined') {
        return (
          <>
            <Tooltip title='Account Settings'>
              <Button shape='circle' icon={<SettingOutlined />} onClick={this.showModal} />
              <Modal
                visible={visible}
                title='ACCOUNT SETTINGS'
                onOk={this.handleOk}
                onCancel={this.handleCancel}
                footer={[
                  <Button key='back' onClick={this.handleCancel}>
                    Cancel
                  </Button>,
                  <Button key='submit' type='primary' loading={loading} onClick={this.handleOk}>
                    Submit
                  </Button>,
                ]}
              >
                <AccountSettingsForm type='update' />
              </Modal>
            </Tooltip>
          </>
        )
      } else {
        return <></>
      }
    } else {
      return <></>
    }
  }
}

export default ModalButton
