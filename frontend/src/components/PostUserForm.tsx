import React, { FC, useState, useEffect } from 'react'
import { Form, Input, Checkbox, Select, Cascader, FormInstance } from 'antd'
import { Typography, Space } from 'antd'
const { Text } = Typography
import {} from '@ant-design/icons'
import { FieldData } from 'rc-field-form/es/interface'

interface PostUserFormProps {
  form: FormInstance
  schools?: any[]
  user?: any
  forDelete?: boolean
}

const PostUserForm: FC<PostUserFormProps> = ({ form, user, schools, forDelete }) => {
  const [fields, setFields] = useState<FieldData[]>([])

  useEffect(() => {
    setFields([
      {
        name: 'userID',
        value: user && user.id,
      },
      {
        name: 'username',
        value: user && user.name,
      },
      {
        name: 'email',
        value: user && user.email,
      },
      {
        name: 'role',
        value: user && user.role,
      },
      {
        name: 'school',
        value: user && user.schoolId,
      },
    ])
  }, [user])

  if (forDelete) {
    return (
      <Form
        form={form}
        fields={fields}
        name='basic'
        labelCol={{
          span: 5,
        }}
        wrapperCol={{
          span: 16,
        }}
        initialValues={{
          remember: true,
        }}
        autoComplete='off'
      >
        <Form.Item
          label='USER ID'
          name='userlID'
          rules={[
            {
              required: true,
              message: 'Please input name of your new user!',
            },
          ]}
        >
          <Select style={{ width: '100%' }}>
            <Input />
            <Cascader style={{ width: '70%' }} />
          </Select>
        </Form.Item>

        <Form.Item
          wrapperCol={{
            offset: 5,
            span: 16,
          }}
        >
          <Typography.Title type='warning' level={5} style={{ margin: 0 }}>
            WARNING
          </Typography.Title>

          <Text type='warning'>
            <p>
              Removal of a User will prevent further access to it from its resources and detaching
              their profiles from it indefinitely.
            </p>
          </Text>
        </Form.Item>

        <Form.Item
          name='remember'
          valuePropName='checked'
          wrapperCol={{
            offset: 5,
            span: 16,
          }}
        >
          <Checkbox>I Understand</Checkbox>
        </Form.Item>
      </Form>
    )
  } else {
    return (
      <Form
        form={form}
        fields={fields}
        onFieldsChange={(_, newFields) => setFields(newFields)}
        name='basic'
        labelCol={{
          span: 5,
        }}
        wrapperCol={{
          span: 16,
        }}
        initialValues={{
          remember: true,
        }}
        autoComplete='off'
      >
        {user && (
          <Form.Item
            label='USER ID'
            name='userID'
            rules={[
              {
                required: false,
                message: 'A new ID is being generated!',
              },
            ]}
          >
            <Input disabled={true} placeholder={'0000-0000-0000-0000'} />
          </Form.Item>
        )}

        <Form.Item
          label='USERNAME'
          name='username'
          rules={[
            {
              required: true,
              message: 'Please input name of the user!',
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label='EMAIL'
          name='email'
          rules={[
            {
              required: true,
              message: 'Please input email of the user!',
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label='ROLE'
          name='role'
          rules={[
            {
              required: true,
              message: 'Please input role of the user!',
            },
          ]}
        >
          <Select style={{ width: '100%' }} placeholder='Select a role'>
            <Select.Option value='Global Administrator'>Global Administrator</Select.Option>
            <Select.Option value='School Administrator'>School Administrator</Select.Option>
            <Select.Option value='Educator'>Educator</Select.Option>
            <Select.Option value='Student'>Student</Select.Option>
          </Select>
        </Form.Item>

        <Form.Item
          label='SCHOOL'
          name='school'
          rules={[
            {
              required: true,
              message: 'Please choose a school!',
            },
          ]}
        >
          <Select
            style={{ width: '100%' }}
            showSearch
            placeholder='Select a school'
            optionFilterProp='children'
            filterOption={(input, option) => {
              const optionText = Array.isArray(option.children)
                ? option.children.join('')
                : option.children
              return optionText.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }}
          >
            {schools.map((school) => (
              <Select.Option value={school.id}>{school.name}</Select.Option>
            ))}
          </Select>
        </Form.Item>
      </Form>
    )
  }
}

export default PostUserForm
