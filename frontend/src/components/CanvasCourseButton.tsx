import React, { FC } from 'react'
import { Avatar, Button } from 'antd'
import { PlusSquareOutlined } from '@ant-design/icons'
import ModalButton from './ModalButton'
import paths from '../config/paths'

/*
This Button navigates to the relevant Canvas LMS course for the user.
*/

const CanvasCourseButton: FC = (props) => {
  const hasCanvasAccess = props.user && props.user.role && props.user.role != 'Global Administrator'
  const path = paths.canvas + '/courses/' + props.course.id

  if (hasCanvasAccess) {
    return (
      <div className='user-element-container-button'>
        <a href={path} target='_blank'>
          <Button type='danger' className='redirect-button'>
            Go to course <PlusSquareOutlined />
          </Button>
        </a>
      </div>
    )
  } else {
    return <></>
  }
}

export default CanvasCourseButton
