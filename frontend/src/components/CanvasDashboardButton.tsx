import React, { FC } from 'react'
import { Avatar, Button } from 'antd'
import { PlusSquareOutlined } from '@ant-design/icons'
import ModalButton from './ModalButton'
import paths from '../config/paths'

/*
This Button navigates to the relevant Canvas LMS account for the user.
*/

const CanvasDashboardButton: FC = (props) => {
  const hasCanvasAccess = props.user && props.user.role && props.user.role != 'Global Administrator'

  if (hasCanvasAccess) {
    return (
      <div className='user-element-container-button'>
        <a href={paths.canvas} target='_blank'>
          <Button type='danger' className='redirect-button'>
            Canvas Dashboard
            <PlusSquareOutlined />
          </Button>
        </a>
      </div>
    )
  } else {
    return <></>
  }
}

export default CanvasDashboardButton
