import React, { FC, useEffect, useState } from 'react'
import { Form, Input, Checkbox, Select, FormInstance } from 'antd'
import { Typography } from 'antd'
const { Text } = Typography
import {} from '@ant-design/icons'
import { FieldData } from 'rc-field-form/es/interface'

interface PostSchoolFormProps {
  form: FormInstance
  administrators?: any[]
  school?: any
  forDelete?: boolean
}

const PostSchoolForm: FC<PostSchoolFormProps> = ({ form, school, administrators, forDelete }) => {
  const [fields, setFields] = useState<FieldData[]>([])

  useEffect(() => {
    setFields([
      {
        name: 'schoolID',
        value: school && school.id,
      },
      {
        name: 'schoolName',
        value: school && school.name,
      },
      {
        name: 'address',
        value: school && school.address && school.address.address,
      },
      {
        name: 'country',
        value: school && school.address && school.address.country,
      },
      {
        name: 'state',
        value: school && school.address && school.address.state,
      },
      {
        name: 'city',
        value: school && school.address && school.address.city,
      },
      {
        name: 'phone',
        value: school && school.phones && school.phones.phone,
      },
      {
        name: 'schoolAdministrator',
        value: school && school.adminId,
      },
      {
        name: 'deletionConfirm',
        value: false,
      },
    ])
  }, [school])

  if (forDelete === true) {
    return (
      <Form
        form={form}
        fields={fields}
        onFieldsChange={(_, newFields) => setFields(newFields)}
        name='basic'
        labelCol={{
          span: 5,
        }}
        wrapperCol={{
          span: 16,
        }}
        initialValues={{
          confirm: false,
        }}
        autoComplete='off'
      >
        <Form.Item
          label='SCHOOL ID'
          name='schoolID'
          rules={[
            {
              required: true,
              message: 'Please input name of your new school!',
            },
          ]}
        >
          <Input disabled={true} />
        </Form.Item>

        <Form.Item
          wrapperCol={{
            offset: 5,
            span: 16,
          }}
        >
          <Typography.Title type='warning' level={5} style={{ margin: 0 }}>
            WARNING
          </Typography.Title>

          <Text type='warning'>
            <p>
              Removal of a School will prevent further access to it from its resources and detaching
              their profiles from it indefinitely.
            </p>
          </Text>
        </Form.Item>

        <Form.Item
          name='deletionConfirm'
          valuePropName='checked'
          wrapperCol={{
            offset: 5,
            span: 16,
          }}
          rules={[
            {
              required: true,
              transform: (value) => value || undefined,
              type: 'boolean',
              message: 'Please tick the checkbox to confirm your action!',
            },
          ]}
        >
          <Checkbox>I Understand</Checkbox>
        </Form.Item>
      </Form>
    )
  } else {
    return (
      <Form
        form={form}
        fields={fields}
        onFieldsChange={(_, newFields) => setFields(newFields)}
        name='basic'
        labelCol={{
          span: 5,
        }}
        wrapperCol={{
          span: 16,
        }}
        initialValues={{
          remember: true,
        }}
        autoComplete='off'
      >
        {school && (
          <Form.Item
            label='SCHOOL ID'
            name='schoolID'
            rules={[
              {
                required: false,
                message: 'A new ID is being generated!',
              },
            ]}
          >
            <Input disabled={true} placeholder={'0000-0000-0000-0000'} />
          </Form.Item>
        )}

        <Form.Item
          label='SCHOOL'
          name='schoolName'
          rules={[
            {
              required: true,
              message: 'Please input name of your school!',
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label='ADDRESS'
          name='address'
          rules={[
            {
              required: true,
              message: 'Please input street address of your school!',
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label='COUNTRY'
          name='country'
          rules={[
            {
              required: true,
              message: 'Please input country of your school!',
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label='STATE'
          name='state'
          rules={[
            {
              required: true,
              message: 'Please input state of your school!',
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label='CITY'
          name='city'
          rules={[
            {
              required: true,
              message: 'Please input city of your school!',
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label='PHONE'
          name='phone'
          rules={[
            {
              required: true,
              message: 'Please input phone number of your school!',
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label='ADMINISTRATOR'
          name='schoolAdministrator'
          rules={[
            {
              required: true,
              message: 'Please choose the administrator of your school!',
            },
          ]}
        >
          <Select
            style={{ width: '100%' }}
            showSearch
            placeholder='Select a person'
            optionFilterProp='children'
            filterOption={(input, option) => {
              const optionText = Array.isArray(option.children)
                ? option.children.join('')
                : option.children
              return optionText.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }}
          >
            {administrators.map((user) => (
              <Select.Option value={user.id}>
                {user.name} ({user.email})
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
      </Form>
    )
  }
}

export default PostSchoolForm
