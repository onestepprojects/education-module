import React, { FC } from 'react'
import { Divider, Table, Tag, Space, Avatar } from 'antd'
import {
  SettingOutlined,
  CloseCircleFilled,
  InfoCircleOutlined,
  UserOutlined,
} from '@ant-design/icons'
import CanvasCourseButton from './CanvasCourseButton'

/*
This class serves as the tables to display the data for each role
*/

const CanvasTable: FC = (props) => {
  const uid = props.user ? props.user.id : null
  const userType = props.user ? props.user.role : null

  if (userType == 'Global Administrator') {
    return (
      <div>
        <Divider className='user-role-container' orientation='left'>
          Schools
        </Divider>
        <div className='user-container'>
          <Table columns={s(schoolColumns)} dataSource={SCHOOLS} />
        </div>

        <Divider className='user-role-container' orientation='left'>
          Users
        </Divider>
        <div className='user-container'>
          <Table columns={s(userColumns)} dataSource={USERS} />
        </div>
      </div>
    )
  } else if (userType == 'School Administrator') {
    return (
      <div>
        <Divider className='user-role-container' orientation='left'>
          Educators
        </Divider>
        <div className='user-container'>
          <Table
            columns={t(props.user, s(userColumns))}
            dataSource={filterEducatorsFromUsers(USERS)}
          />
        </div>

        <Divider className='user-role-container' orientation='left'>
          Students
        </Divider>
        <div className='user-container'>
          <Table columns={userColumns} dataSource={filterStudentsFromUsers(USERS)} />
        </div>
      </div>
    )
  } else if (userType == 'Educator') {
    return (
      <div>
        <Divider className='user-role-container' orientation='left'>
          Current Courses
        </Divider>
        <div className='user-container'>
          <Table
            columns={t(props.user, taughtCourseColumns)}
            dataSource={filterCompletedCoursesByEducator(COURSES, uid, false)}
          />
        </div>

        <Divider className='user-role-container' orientation='left'>
          Previous Courses
        </Divider>
        <div className='user-container'>
          <Table
            columns={taughtCourseColumns}
            dataSource={filterCompletedCoursesByEducator(COURSES, uid, true)}
          />
        </div>
      </div>
    )
  } else if (userType == 'Student') {
    return (
      <div>
        <Divider className='user-role-container' orientation='left'>
          Current Courses
        </Divider>
        <div className='user-container'>
          <Table
            columns={t(props.user, enrolledCourseColumns)}
            dataSource={filterCompletedCoursesByStudent(COURSES, uid, false)}
          />
        </div>

        <Divider className='user-role-container' orientation='left'>
          Previous Course
        </Divider>
        <div className='user-container'>
          <Table
            columns={enrolledCourseColumns}
            dataSource={filterCompletedCoursesByStudent(COURSES, uid, false)}
          />
        </div>
      </div>
    )
  } else {
    ;<></>
  }

  return <></>
}

// MODELS
/* The data models below represent those returned from our API calls */

const SCHOOLS = [
  {
    id: 'test-school-001',
    name: 'Harvard University',
    streetAddress: '123 4th Street',
    city: 'Boston',
    stateOrProvince: 'MA, 12345',
    country: 'USA',
    adminId: 'test-user-001',
  },
]

export const USERS = [
  {
    id: 'test-user-001',
    name: 'HES_User1',
    email: 'hu1@g.harvard.edu',
    role: 'School Administrator',
    schoolId: 'test-school-001',
  },
  {
    id: 'test-user-002',
    name: 'HES_User2',
    email: 'hu2@g.harvard.edu',
    role: 'Educator',
    schoolId: 'test-school-001',
  },
  {
    id: 'test-user-003',
    name: 'HES_User3',
    email: 'hu3@g.harvard.edu',
    role: 'Student',
    schoolId: 'test-school-001',
  },
  {
    id: 'test-user-004',
    name: 'HES_User4',
    email: 'hu4@g.harvard.edu',
    role: 'Student',
    schoolId: 'test-school-001',
  },
  {
    id: 'test-user-005',
    name: 'HES_User5',
    email: 'hu5@g.harvard.edu',
    role: 'Student',
    schoolId: 'test-school-001',
  },
]

export const COURSES = [
  {
    id: 'test-course-001',
    title: 'Intro to Biology',
    category: 'BIOL',
    educatorId: 'test-user-002',
    progress: 50,
    studentIds: ['test-user-003', 'test-user-004', 'test-user-005'],
  },
  {
    id: 'test-course-002',
    title: 'Intro to Chemistry',
    category: 'CHEM',
    educatorId: 'test-user-002',
    progress: 50,
    studentIds: ['test-user-003', 'test-user-004', 'test-user-005'],
  },
  {
    id: 'test-course-003',
    title: 'Intro to Physics',
    category: 'PHYS',
    educatorId: 'test-user-002',
    progress: 50,
    studentIds: ['test-user-003', 'test-user-004', 'test-user-005'],
  },
  {
    id: 'test-course-004',
    title: 'Intro to Mathematics',
    category: 'MATH',
    educatorId: 'test-user-002',
    progress: 50,
    studentIds: ['test-user-003', 'test-user-004', 'test-user-005'],
  },
  {
    id: 'test-course-005',
    title: 'Intro to Computer Science',
    category: 'CSCI',
    educatorId: 'test-user-002',
    progress: 50,
    studentIds: ['test-user-003', 'test-user-004', 'test-user-005'],
  },
  {
    id: 'test-course-006',
    title: 'Intro to Biology',
    category: 'BIOL',
    educatorId: 'test-user-002',
    progress: 100,
    studentIds: ['test-user-003', 'test-user-004', 'test-user-005'],
  },
  {
    id: 'test-course-007',
    title: 'Intro to Chemistry',
    category: 'CHEM',
    educatorId: 'test-user-002',
    progress: 100,
    studentIds: ['test-user-003', 'test-user-004', 'test-user-005'],
  },
  {
    id: 'test-course-008',
    title: 'Intro to Physics',
    category: 'PHYS',
    educatorId: 'test-user-002',
    progress: 100,
    studentIds: ['test-user-003', 'test-user-004', 'test-user-005'],
  },
]

// FILTERS
/* The filters below help organize overlapping data types to display on in the table */

function filterEducatorsFromUsers(users) {
  let result = []
  for (let i = 0; i < users.length; i++) {
    if (users[i].role && users[i].role == 'Educator') {
      result.push(users[i])
    }
  }
  return result
}

function filterStudentsFromUsers(users) {
  let result = []
  for (let i = 0; i < users.length; i++) {
    if (users[i].role && users[i].role == 'Student') {
      result.push(users[i])
    }
  }
  return result
}

function filterCompletedCoursesByEducator(courses, eid, complete) {
  let result = []
  for (let i = 0; i < courses.length; i++) {
    if ((courses[i].progress == 100) == complete) {
      if (courses[i].educatorId && courses[i].educatorId == eid) {
        result.push(courses[i])
      }
    }
  }
  return result
}

function filterCompletedCoursesByStudent(courses, sid, complete) {
  let result = []
  for (let i = 0; i < courses.length; i++) {
    if ((courses[i].progress == 100) == complete) {
      if (courses[i].studentIds && courses[i].studentIds.includes(sid)) {
        result.push(courses[i])
      }
    }
  }
  return result
}

// COLUMNS
/* The models below represent the colum types within our tables */

const schoolColumns = [
  {
    title: () => (
      <a>
        <InfoCircleOutlined className='padded' />
      </a>
    ),
    dataIndex: '',
    key: 'x',
    render: () => (
      <a>
        <Avatar size={20} icon={<UserOutlined />} />
      </a>
    ),
  },
  {
    title: 'ID',
    dataIndex: 'id',
    filters: [],
    // specify the condition of filtering result
    // here is that finding the name started with `value`
    onFilter: (value, record) => record.id.indexOf(value) === 0,
    sorter: (a, b) => a.id.length - b.id.length,
    sortDirections: ['descend'],
  },
  {
    title: 'NAME',
    dataIndex: 'name',
    defaultSortOrder: 'descend',
    sorter: (a, b) => a.name - b.name,
  },
  {
    title: 'STREET',
    dataIndex: 'streetAddress',
    defaultSortOrder: 'descend',
    sorter: (a, b) => a.streetAddress - b.streetAddress,
  },
  {
    title: 'CITY',
    dataIndex: 'city',
    defaultSortOrder: 'descend',
    sorter: (a, b) => a.city - b.city,
  },
  {
    title: 'STATE/ZIPCODE',
    dataIndex: 'stateOrProvince',
    defaultSortOrder: 'descend',
    sorter: (a, b) => a.stateOrProvince - b.stateOrProvince,
  },
  {
    title: 'COUNTRY',
    dataIndex: 'country',
    defaultSortOrder: 'descend',
    sorter: (a, b) => a.country - b.country,
  },
  {
    title: 'ADMIN',
    dataIndex: 'adminId',
    defaultSortOrder: 'descend',
    sorter: (a, b) => a.adminId - b.adminId,
  },
]

const userColumns = [
  {
    title: () => (
      <a>
        <InfoCircleOutlined className='padded' />
      </a>
    ),
    dataIndex: '',
    key: 'x',
    render: () => (
      <a>
        <Avatar size={20} icon={<UserOutlined />} />
      </a>
    ),
  },
  {
    title: 'ID',
    dataIndex: 'id',
    filters: [],
    // specify the condition of filtering result
    // here is that finding the name started with `value`
    onFilter: (value, record) => record.id.indexOf(value) === 0,
    sorter: (a, b) => a.id.length - b.id.length,
    sortDirections: ['descend'],
  },
  {
    title: 'NAME',
    dataIndex: 'name',
    defaultSortOrder: 'descend',
    sorter: (a, b) => a.name - b.name,
  },
  {
    title: 'EMAIL',
    dataIndex: 'email',
    defaultSortOrder: 'descend',
    sorter: (a, b) => a.email - b.email,
  },
  {
    title: 'ROLE',
    dataIndex: 'role',
    defaultSortOrder: 'descend',
    sorter: (a, b) => a.role - b.role,
  },
  {
    title: 'SCHOOL',
    dataIndex: 'schoolId',
    defaultSortOrder: 'descend',
    sorter: (a, b) => a.schoolId - b.schoolId,
  },
]

function canSeeCanvas(user) {
  if (user && user.role && user.role != 'Global Administrator') {
    return {
      title: 'CANVAS',
      dataIndex: '',
      key: 'x',
      render: () => (
        <a>
          <CanvasCourseButton user={user} />
        </a>
      ),
    }
  } else {
    return {}
  }
}

function s(table) {
  let hasSettings = false
  for (let i = 0; i < table.length; i++) {
    if (table[i].title == 'SETTINGS') {
      hasSettings = true
    }
  }
  if (!hasSettings) {
    table.push({
      title: 'SETTINGS',
      dataIndex: '',
      key: 'x',
      render: () => (
        <a>
          <SettingOutlined className='padded' />
          <CloseCircleFilled className='padded' />
        </a>
      ),
    })
  }
  return table
}

function t(user, table) {
  let hasCanvas = false
  for (let i = 0; i < table.length; i++) {
    if (table[i].title == 'CANVAS') {
      hasCanvas = true
    }
  }
  if (!hasCanvas) {
    let x = table.length - 1
    let y = 0
    table.splice(x, y, canSeeCanvas(user))
    if (table[x + 1].title != 'SETTINGS') {
      let tmp = table[x]
      table[x] = table[x + 1]
      table[x + 1] = tmp
    }
  }
  return table
}

const taughtCourseColumns = [
  {
    title: () => (
      <a>
        <InfoCircleOutlined className='padded' />
      </a>
    ),
    dataIndex: '',
    key: 'x',
    render: () => (
      <a>
        <Avatar size={20} icon={<UserOutlined />} />
      </a>
    ),
  },
  {
    title: 'ID',
    dataIndex: 'id',
    filters: [],
    // specify the condition of filtering result
    // here is that finding the name started with `value`
    onFilter: (value, record) => record.id.indexOf(value) === 0,
    sorter: (a, b) => a.id.length - b.id.length,
    sortDirections: ['descend'],
  },
  {
    title: 'TITLE',
    dataIndex: 'title',
    defaultSortOrder: 'descend',
    sorter: (a, b) => a.title - b.title,
  },
  {
    title: 'CATEGORY',
    dataIndex: 'category',
    defaultSortOrder: 'descend',
    sorter: (a, b) => a.category - b.category,
  },
  {
    title: 'ENROLLMENT',
    dataIndex: 'studentIds',
    defaultSortOrder: 'descend',
    sorter: (a, b) => a.studentIds.length - b.studentIds.length,
  },
  {
    title: 'PROGRESS',
    dataIndex: 'progress',
    defaultSortOrder: 'descend',
    sorter: (a, b) => a.progress - b.progress,
  },
]

const enrolledCourseColumns = [
  {
    title: () => (
      <a>
        <InfoCircleOutlined className='padded' />
      </a>
    ),
    dataIndex: '',
    key: 'x',
    render: () => (
      <a>
        <Avatar size={20} icon={<UserOutlined />} />
      </a>
    ),
  },
  {
    title: 'ID',
    dataIndex: 'id',
    filters: [],
    // specify the condition of filtering result
    // here is that finding the name started with `value`
    onFilter: (value, record) => record.id.indexOf(value) === 0,
    sorter: (a, b) => a.id.length - b.id.length,
    sortDirections: ['descend'],
  },
  {
    title: 'TITLE',
    dataIndex: 'title',
    defaultSortOrder: 'descend',
    sorter: (a, b) => a.title - b.title,
  },
  {
    title: 'CATEGORY',
    dataIndex: 'category',
    defaultSortOrder: 'descend',
    sorter: (a, b) => a.category - b.category,
  },
  {
    title: 'TEACHER',
    dataIndex: 'educatorId',
    defaultSortOrder: 'descend',
    sorter: (a, b) => a.educatorId - b.educatorId,
  },
  {
    title: 'PROGRESS',
    dataIndex: 'progress',
    defaultSortOrder: 'descend',
    sorter: (a, b) => a.progress - b.progress,
  },
]

function onChange(pagination, filters, sorter, extra) {
  console.log('params', pagination, filters, sorter, extra)
}
export default CanvasTable
