import React, { FC } from 'react'
import { Breadcrumb } from 'antd'
import { MenuOutlined } from '@ant-design/icons'

/*
This class helps the user identify what should they are a part of
*/
const HeaderPanel: FC = (props) => {
  const details =
    props.user && props.user.role != 'Global Administrator' ? props.user.schoolId : '*'

  return (
    <div>
      <table>
        <tbody>
          <tr>
            <td>
              <div className='padding-from-above'>
                <Breadcrumb>
                  <Breadcrumb.Item>Education</Breadcrumb.Item>
                  <Breadcrumb.Item>
                    <a href='#'> {details} </a>
                  </Breadcrumb.Item>
                </Breadcrumb>
              </div>
            </td>
          </tr>
          <tr>
            <td>
              <h3 className='onestep-tiny-header-fix'>Dashboard</h3>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  )
}

export default HeaderPanel
