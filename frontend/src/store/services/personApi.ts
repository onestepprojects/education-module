import type { Person } from '../types'
import { api } from './api'

export const personApi = api.injectEndpoints({
  overrideExisting: true,

  endpoints: (builder) => ({
    getPersonsOfOrganization: builder.query<Person[], string>({
      query: (orgId) => `org/persons/get-by-parent/${orgId}`,
      providesTags: ['Person']
    })
  })
})

export const { useGetPersonsOfOrganizationQuery } = personApi
