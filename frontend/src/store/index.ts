export * from './hooks'
export * from './features'
export * from './services'
export * from './store'
export * from './types'

import { v4 as uuidv4 } from 'uuid'

export const getSchools = () => {
  let schoolList = []
  return fetch('http://localhost:8080/schools')
    .then((res) => res.json())
    .then((data) => {
      console.log(data)
      return data
    })
    .catch((e) => {
      console.log(`[GET] fetching error: `, e)
    })
}

export const addSchool = (name) => {
  const options = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
      id: uuidv4(),
      name: name,
      streetAddress: null,
      city: null,
      stateOrProvince: null,
      country: null,
    }),
  }

  fetch('http://localhost:8080/schools', options)
    .then((res) => res.json())
    .then((data) => {
      console.log('Added ' + name)
    })
    .catch((e) => {
      console.log(`[ADD] fetching error: `, e)
    })
}

export const updateSchool = (id) => {
  const options = {
    method: 'PUT',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
      id: id,
      name: 'updatedSchoolV1',
      streetAddress: null,
      city: null,
      stateOrProvince: null,
      country: null,
    }),
  }

  fetch('http://localhost:8080/schools/' + id, options)
    .then((res) => res.json())
    .then((data) => {
      console.log('Updated ' + id)
    })
    .catch((e) => {
      console.log(`[UPDATE] fetching error: `, e)
    })
}

export const removeSchool = (id) => {
  const options = {
    method: 'DELETE',
  }

  fetch('http://localhost:8080/schools/' + id, options)
    .then(() => console.log('Delete successful'))
    .catch((e) => {
      console.log(`[DELETE] fetching error: `, e)
    })
}
