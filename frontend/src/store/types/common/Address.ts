export interface Address {
    address: string
    city: string
    country: string
    district: string
    postalCode: string
    state: string
  }
