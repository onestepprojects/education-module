# OneStep Education Module

## Table of Contents
- [Team Members](#team-members)
- [Product Owner](#product-owner)
- [Faculty](#faculty)
- [Technologies](#technologies)
- [Project Overview](#project-overview)
- [Education Module Components](#education-components)
    - [Dashboard](#dashboard-component)
    - [Surveys](#survey-component)
    - [Discussion Board](#discussion-board-component)
    - [Ticketing](#ticketing-component)
    - [Mentorship](#mentorship-component)
    - [Curriculum](#curriculum-component)
	
## Team Members
* Simeon Greene
* Chris Crane
* Truman Biro
* Christopher Jones
* Sri Vatsavai
* Babak Damadi
* Ernst Junior Barbier
* David Wilkie

## Product Owner
* Eric Gieseke

## Faculty
* Eric Gieseke

## Technologies
* Programming Languages
    - Java
    - Javascript
* Data Persistence
    - DynamoDB
* RESTful API
    - DropWizard
* UI
    - React
* IDE
    - IntelliJ
    - VSCode
* Server Environment
    - AWS
* Deployment
    - Docker
    - Kubernetes
    - Helm Charts

## Project Overview


## How-to

### Setup (OSX)

#### Maven

```
brew install maven
```

#### Docker

If you intend to use Docker, on a Mac please use Brew, specifically the cask:

```
brew install --cask docker
```

Please ensure the ‘—cask’ is used, do _not_ try to install docker-machine/virtualbox separately as it will likely cause issues.

After this is complete, you should start Docker from the launchpad.

It will ask for privileged access, provide it.

A whale icon should appear in your top task bar. Wait for some time and eventually the Docker app will load and get you to run a test command called ‘getting started’. Copy-paste it and run it, everything should work.

At this point, you should be able to proceed with the ‘Build with Docker’ instructions below.


### Back-end

The backend folder contains the code for running the education-module REST server and persistence (Dynamo DB). You can run the server as a jar file or with Docker.

#### Build, Test and Run

##### Preconditions

(**FOR UNIT TESTS ONLY**) The JUnit tests run against a local dynamodb instance and not against any instance in AWS. Therefore a local dynamodb instance must be running whenever the unit tests are run. This can be easily run with docker

```
docker run -p 8000:8000 amazon/dynamodb-local
```

**NOTE**: You can always skip tests by passing the parameter `-DskipTests` to maven during the build.

##### Build

To build the backend jar file - `education-1.0-SNAPSHOT.jar` - do the following:

```
cd backend  
mvn package 
```
The `mvn package` command will also run all tests. To skip tests - for a faster build - use:

```
mvn package -DskipTests
```

**(OPTIONAL) Build with Docker**

Using the docker container is not mandatory as the backend can run with Java. The docker image can be built with maven commands.

```
cd backend
mvn clean install
```
This might take some time, especially on the first run, but at the end you should see the docker image in your image list. To check run:

```
docker images
```

**(Optional) Deploying A Docker Image**

If you need to deploy the docker image to the remote gitlab repository follow the steps in this section.
Before deploying an image for the very first time you **MUST** login with the remote docker registry. Do the following:

```
docker login registry.gitlab.com
```

You will be prompted for both username and password. These are your gitlab credentials. This login step is only required once.

To push a new version of the docker image to the shared repository you can use:

```
mvn deploy
```

##### Testing

Unit tests are run automatically when `mvn package` or `mvn install` is invoked in the build step. One pre-requisite of tests is that DynamoDB is up and running **Locally**. Unit tests do not run against the DynamoDB in AWS. Dynamo DB can be run locally in several ways but the easiest way is to use docker:

```
docker run -p 8000:8000 amazon/dynamodb-local
```

This will launch dynamodb on your host and expose port 8000. You should be all set to run the tests at this point. In addition to running tests using `mvn package` you can also run tests directly using `mvn test`. 


##### Running

The backend can be run from the command line using the jar file (previously built with `mvn package`) **OR** the docker image. Previously built with `docker build`.

**Pre-requisite**

1) You will need to have the Organization Service running since the Education Service communicates with it to create Organizations and People. You can run the Organization Service as a docker image. **You should verify** the latest version in the gitlab registry `docker run -e AWS_ACCESS_KEY_ID=<ACCESS KEY> -e AWS_SECRET_ACCESS_KEY=<SECRET KEY> -e DDB_REGION_NAME=<region> -it -p 9090:8080 registry.gitlab.com/onestepprojects/organization-module/organization-service:<version>` . 
Replace `<ACCESS KEY>`, `<SECRET KEY>` and `<region>` with values you find in ~/onestep-server-env.properties. Replace `<version>` with the latest version of the Organization docker you can find in the [docker container registry](https://gitlab.com/onestepprojects/organization-module/container_registry/2362799). 

**Note:** see the front-end [README.md](./frontend/README.md) to update the `REACT_APP_ORG_MODULE_API_URL` environment variable.

2) You will need to have an API token for Canvas. You can generate this yourself or ask a fellow teammate/admin.
3) To run the server locally you have the option of connecting to AWS for Dynamo DB (the default behavior), or running against your local DynamoDB instance. 
In either case you will the right environment variables set to run the process. You have two options for passing these env variables: as a file or on the command line.

#### File Environment Variables
 **NOTE: This is NOT An Option for Docker. ONLY for Java**
 Environment variables can be loaded from a file in your home directory. You will need the file `onestep-server-env.properties` in your home directory (**NOTE**: The file may have a different name but it is a file with all required environment variavles). This file should be provided by the Onestep Architect but at a minimum it will contain the following properties:

* **DDB_REGION_NAME** - This is the region for AWS
* **AWS_ACCESS_KEY_ID** - This is the access key for AWS. A must have for connecting to AWS 
* **AWS_SECRET_ACCESS_KEY** - This is the AWS secret.
* **CANVAS_API_TOKEN** - This is the API token for connecting to Canvas.

**NOTE**: Keep this file in your home directory only and never share it or commit it to the project.

In addition to the above properies, if you want to connect to a DynamoDB Local Instance you will need the following properties:

* **DDB_USE_LOCAL** - If this property is set to true then a local connection to your DynamoDB will be attempted.

* **DDB_HOST** - The host on which DynamoDB Local is running. This property is optional and defaults to localhost if not specified. If **DDB_USE_LOCAL** is missing or set to false then this property is ignored.

* **DDB_PORT** - The port on which DynamoDB Local is running. This property is optional and defaults to 8000 if not specified. If **DDB_USE_LOCAL** is missing or set to false then this property is ignored.

* **OPP_HOST** - This is the host for where the Organization Service is running. If ommitted this will be `localhost`

* **OPP_PORT** - The port that the Organization Service is bound to. If ommitted this will be `8080`


#### Passing Env Variables on the Command Line

Both the docker and jar file versions can handle environments being passed on the command line. 

**For Jar File (Java)**
Pass the env variable using the `-D` parameter. For example, if you wanted to specify the port that the org service is bound to:

```
java -jar -DOPP_PORT=9090 target/education-1.0-SNAPSHOT.jar server
```
In the above example the education service will start and expect to find the Organization Service at localhost:9090 (localhost is a default value)

**For Docker Runs**
Pass env variables using the `-e` flag. For example if you wanted to let the docker container know that the Organization service is bound to port 9090

```
docker run -e OPP_PORT=9090 -p 8080:8080 education-service:1.0
```

**Running the jar file**

```
cd backend
java -jar target/education-1.0-SNAPSHOT.jar server
```

**(Optional) Running via Docker**

```
cd backend
docker run docker run -p 8080:8080 education-service:1.0
```

**Validating Server**
To verify that the server is running you should try hitting the endpoint with a tool like `curl` or `Postman`. Here's an example curl request:

```
curl http://localhost:8080/schools
```

Example output (NOTE: Your output may differ but there should be no errors):

```
{"schools":[{"id":"4f7d4adc-8560-41ae-bb9e-41118acffb97","name":"TestSchool","streetAddress":null,"city":"Boston","stateOrProvince":"MA","country":null}]}%   
```
**(Optional) Creating tables on Local DynamoDB**
If you need to create tables in the local database you can use this handy example.
```
aws dynamodb create-table --table-name School --attribute-definitions AttributeName=id,AttributeType=S --key-schema AttributeName=id,KeyType=HASH --endpoint-url http://localhost:8000 --billing-mode PAY_PER_REQUEST
```
#### CRUD Examples

NOTE: In CRUD operations replace `<token>` with a valid auth token. You can get tokens from the UI

**Example: CRUD Operations on School**
```
curl -i -X GET http://localhost:8080/schools
curl -i -X GET http://localhost:8080/schools/1
curl -i -X POST http://localhost:8080/schools -H 'Content-Type: application/json' -H  'Authorization: Bearer <token>' -d {"name": “Sample University","address": {"address": "200 Harvard Square","state":"MA","city":"Boston"},"phones": {"sat_phone":"1111", "phone": "1111", "mobile":"1111"}}
curl -i -X DELETE http://localhost:8080/schools/781863bc-f825-48f2-9af3-78177bbff9c1 -H 'Content-Type: application/json' -H 'Authorization: Bearer <token>'
```

**Example: CRUD Operations on Person**

```
curl -i -X GET http://localhost:8080/users/
curl -i -X GET http://localhost:8080/users/test-user-001
curl -i -X POST http://localhost:8080/users -H 'Content-Type: application/json' -d '{"id":"test-user-003","name":"HES_User3","email":"hu3@g.harvard.edu","role":"Student","schoolId":"test-school-001"}'
curl -i -X PUT http://localhost:8080/users/test-user-001 -H 'Content-Type: application/json' -d '{"id":"test-user-001", "name":"user-1-new"}'
curl -i -X PUT http://localhost:8080/users/test-user-001 -H 'Content-Type: application/json' -d '{"id":"test-user-001", "email":"user-1-new@email.com"}'
curl -i -X DELETE http://localhost:8080/users/test-user-001  
```

**Example: CRUD Operations on Courses**

```
curl -i -X GET http://localhost:8080/courses/
curl -i -X GET http://localhost:8080/courses/test-course-008
```


**Example: Registering a User: associates person to organization and create user in canvas**

```
curl -i -X POST http://localhost:8080/schools/registerUser/{schoolId}/{userId}/{role}/{userName}/{email} -H 'Authorization: Bearer <token>'
```



**END POINT for getting registered/unregistered users**
```
curl -i -X GET http://localhost:8080/schools/users?registered=true -- to get a list of registered users
curl -i -X GET http://localhost:8080/schools/users?registered=false -- to get a list of unregistered users
```

**GET canvas user id**
```
curl -i -X GET http://localhost:8080/schools/canvasUserId/{orgUserId}
```

**GET canvas account id**
```
curl -i -X GET http://localhost:8080/schools/canvasAccountId/{orgId}
```

**Swagger Support**
```
For accessing the Swagger API specification: http://localhost:8080/openapi.json
For accessing the Swagger UI: http://localhost:8080/api-docs/
```
#### Kubernetes Deployment
The Education module backend can be deployed to any Kubernetes cluster using [Helm](https://helm.sh/). The `helm` charts for the education module backend are located in the `deploy` folder. In producation [Amazon EKS](https://aws.amazon.com/eks/) is the Kubernetes cluster of choice. You will need priveleges to deploy to this cluster and once you have it, configure your kubectl config to point to it. After that it's as simple as 
```
cd deploy/helm
helm install -f education/values/capstone.yml education-service ./education 
```

### Front-End
Please refer to the frontend README under the /frontend directory.